package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.api.EnderecoApi;
import com.cidadao.entity.DadosPessoais;

@ApplicationScoped
public class DadosPessoaisService {
    Logger logger = Logger.getLogger(DadosPessoaisService.class.getName());
    
    @RestClient
    EnderecoApi enderecoApi;


    @ConfigProperty(name = "dados.pessoais.abortar.consulta") 
    Boolean abortarConsulta;

    @ConfigProperty(name = "dados.pessoais.delay.consulta") 
    Long delayConsulta;


    static Map<String,DadosPessoais> dados = new HashMap<String,DadosPessoais>();
    public DadosPessoais doGetByFilter(String cpf){
       
        if(abortarConsulta != null && abortarConsulta){
            logger.info("Abortando....");
            throw new RuntimeException();
        }else if(delayConsulta != null && delayConsulta.longValue() >0){
            try {
                logger.info("Aplicando Delay de "+ delayConsulta.longValue());
                Thread.sleep(delayConsulta);
            } catch (Exception e) {
                throw new RuntimeException();
            } 
        }


        DadosPessoais dadosPessoais = dados.get(cpf);
        logger.info("Dados Pessoais para o CPF "+cpf+" "+" size dados "+dados.size());
        dadosPessoais.endereco=enderecoApi.doGetByFilter(dadosPessoais.cep);
        return dadosPessoais;
    }
    
    public List<DadosPessoais> doGetAll() {
        List<DadosPessoais> _dados = new ArrayList<DadosPessoais>();
        _dados.addAll(dados.values());
        return _dados;
    }

    @PostConstruct
    private void mountDadosPessoais(){
        for(int i=0;i<=100;i++){
            DadosPessoais dadosPessoais = getDadosPessoais(i);
            dados.put(dadosPessoais.cpf, dadosPessoais);
        }
    }

    private DadosPessoais getDadosPessoais(int sequencia){
        NumberFormat df = new DecimalFormat("00000000000");
        DadosPessoais dadosPessoais = new DadosPessoais();
        dadosPessoais.cpf=df.format(sequencia);

        NumberFormat dfcep = new DecimalFormat("00000000");
        dadosPessoais.cep=dfcep.format(sequencia);

        if(sequencia %2==0){
            dadosPessoais.nome="João Cidadão X"+sequencia;
        }else{
            dadosPessoais.nome="Maria Cidadã X"+sequencia;
        }
        return dadosPessoais;
    }

    

    
}
