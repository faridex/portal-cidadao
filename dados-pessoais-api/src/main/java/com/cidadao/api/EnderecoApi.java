package com.cidadao.api;



import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.cidadao.entity.Endereco;


@Path("/endereco")
@RegisterRestClient
public interface EnderecoApi {

    
    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Endereco doGetByFilter(
        @QueryParam("cep") String cep
    );

}