package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;


import com.cidadao.entity.DadosPessoais;
import com.cidadao.entity.Endereco;

@ApplicationScoped
public class DadosPessoaisService {
    
    static Map<String,DadosPessoais> dados = new HashMap<String,DadosPessoais>();
    public DadosPessoais doRecuperarDadosPessoiasPorCpf(String cpf){
       return dados.get(cpf);
    }

    @PostConstruct
    private void mountAgendamento(){
        for(int i=0;i<100;i++){
            DadosPessoais dadosPessoais = getDadosPessoais(i);
            dados.put(dadosPessoais.cpf, dadosPessoais);
        }
    }

    private DadosPessoais getDadosPessoais(int sequencia){
        NumberFormat df = new DecimalFormat("00000000000");
        DadosPessoais dadosPessoais = new DadosPessoais();
        dadosPessoais.cpf=df.format(sequencia);

        NumberFormat dfcep = new DecimalFormat("00000000");
        dadosPessoais.cep=dfcep.format(sequencia);

        if(sequencia %2==0){
            dadosPessoais.nome="João Cidadão X"+sequencia;
        }else{
            dadosPessoais.nome="Maria Cidadã X"+sequencia;
        }
        dadosPessoais.endereco=getEndereco(sequencia);
        return dadosPessoais;
    }

    private Endereco getEndereco(int sequencia){
        NumberFormat df = new DecimalFormat("00000000");
        Endereco endereco = new Endereco();
        endereco.cep=df.format(sequencia);
        if(sequencia %2==0){
            endereco.logradouro="Avenida vai que vai "+sequencia;
        }else{
            endereco.logradouro="Rua bem Ali número "+sequencia;
        }
        return endereco;
    }

    public List<DadosPessoais> doRecuperarDadosPessoais() {
        List<DadosPessoais> _dados = new ArrayList<DadosPessoais>();
        _dados.addAll(dados.values());
        return _dados;
    }
}
