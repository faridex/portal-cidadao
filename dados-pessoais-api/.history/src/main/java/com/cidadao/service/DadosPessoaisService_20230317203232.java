package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import com.cidadao.entity.Agendamento;
import com.cidadao.entity.DadosPessoais;
import com.cidadao.entity.Endereco;

@ApplicationScoped
public class AgendamentoService {
    
    static Map<String,Agendamento> agendamentos = new HashMap<String,Agendamento>();
    public Agendamento doRecuperarAgendamentoPorCpf(String cpf){
       
        return agendamentos.get(cpf);
    }

    @PostConstruct
    private void mountAgendamento(){
        for(int i=0;i<100;i++){
            Agendamento agendamento = new Agendamento();
            DadosPessoais dadosPessoais = getDadosPessoais(i);
            agendamento.dadosPessoias=dadosPessoais;
            agendamentos.put(dadosPessoais.cpf, agendamento);
        }
    }

    private DadosPessoais getDadosPessoais(int sequencia){
        NumberFormat df = new DecimalFormat("00000000000");
        DadosPessoais dadosPessoais = new DadosPessoais();
        dadosPessoais.cpf=df.format(sequencia);
        if(sequencia %2==0){
            dadosPessoais.nome="João Cidadão X"+sequencia;
        }else{
            dadosPessoais.nome="Maria Cidadã X"+sequencia;
        }
        dadosPessoais.endereco=getEndereco(sequencia);
        return dadosPessoais;
    }

    private Endereco getEndereco(int sequencia){
        NumberFormat df = new DecimalFormat("00000000");
        Endereco endereco = new Endereco();
        endereco.cep=df.format(sequencia);
        if(sequencia %2==0){
            endereco.logradouro="Avenida vai que vai "+sequencia;
        }else{
            endereco.logradouro="Rua bem Ali número "+sequencia;
        }
        return endereco;
    }

    public List<Agendamento> doRecuperarAgendamentos() {
        List<Agendamento> _agendamentos = new ArrayList<Agendamento>();
        _agendamentos.addAll(agendamentos.values());
        return _agendamentos;
    }
}
