package com.cidadao;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cidadao.entity.DadosPessoais;
import com.cidadao.service.DadosPessoaisService;

@Path("/dados-pessoais")
public class DadosPessoaisResource {

    @Inject
    DadosPessoaisService agendamentoService;

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doRecuperarInformacaoPessoalPorCpf(
        @QueryParam("cpf") String cpf
    ) {
        DadosPessoais dados = agendamentoService.doRecuperarDadosPessoiasPorCpf(cpf);
        return Response.ok(dados).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGetDadosPessoais() {
        List<DadosPessoais> agendamentos = agendamentoService.doRecuperarAgendamentos();
        return Response.ok(agendamentos).build();
    }
}