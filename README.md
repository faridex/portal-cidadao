# Para iniciar a App fazendo uso do proxy para fazer uso do CORS
npm run start

# Compilando para gerar Imagem para quay do portal-cidadao-app
ng build

# build imagem portal-cidade-app
podman build --platform linux/amd64 -t quay.io/farid_ahmad/cidadao/portal-cidadao-app:1.0 .

# push para o quay portal-cidade-app
podman push quay.io/farid_ahmad/cidadao/portal-cidadao-app:1.0

# executar imagem portal-cidade-app
docker run -d --name=portal-cidadao-app -p 8080:8080 quay.io/farid_ahmad/cidadao/portal-cidadao-app:1.0



### Loop para simular requisições no ingress
while true; do curl "http://istio-ingressgateway-istio-system.apps.cluster-k4629.k4629.sandbox2984.opentlc.com/iptu?cpf=00000000022" -w '\nTempo %{time_total}\n'; sleep 1; done




## completo 
while true ; do curl "https://portal-cidadao-product-3scale-apicast-staging.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com:443/iptu/filtros?cpf=00000000001&user_key=portal-cidadao-key" -w ' Tempo : %{time_total}'; sleep 1; echo "\n";done;

while true; do curl "http://istio-ingressgateway-istio-system.apps.cluster-k4629.k4629.sandbox2984.opentlc.com/agendamento?cpf=00000000022"; sleep 1; done

## mostrar tempo de resposta em uma requisiçao com o curl
curl -w 'Tempo %{time_total}' "https://portal-cidadao-product-3scale-apicast-staging.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com:443/iptu/filtros?cpf=00000000001&user_key=portal-cidadao-key"


https://portal-cidadao-product-3scale-apicast-staging.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com:443/iptu/filtros?cpf=00000000001&user_key=portal-cidadao-key


https://agendamento-3scale-apicast-staging.apps.cluster-4wnjp.4wnjp.sandbox2918.opentlc.com:443/agendamento?cpf=00000000022&user_key=ae1116a406bae4abe3dac56cb1db20e6

## Build package quarkus

mvn clean package

## Build para em ambiente M1,  força imagem base compativo  com amd64
docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/iptu-api:1.0 .

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/dados-pessoais-api:1.0 .

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/endereco-api:1.0 .

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/pagamento-api:1.0 .

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/aluno-api:1.0 .

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/boletim-api:1.0 .


# build image cadastro-api podman
mvn clean compile package
podman build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/cidadao-api:1.0 .

# build image mobilidade-api podman
mvn clean compile package
podman build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/mobilidade-api:1.0 .


### ipview
docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t faridex/farid_ahmad/ipview:1.0 .


## Push to respository
docker push quay.io/farid_ahmad/cidadao/iptu-api:1.0

docker push quay.io/farid_ahmad/cidadao/dados-pessoais-api:1.0

docker push quay.io/farid_ahmad/cidadao/endereco-api:1.0

docker push quay.io/farid_ahmad/cidadao/pagamento-api:1.0

docker push quay.io/farid_ahmad/cidadao/aluno-api:1.0

docker push quay.io/farid_ahmad/cidadao/boletim-api:1.0

## Aplicar deploy agenda-api-vi
oc create/replace -f deployment-iptu-api-v1.yaml

## Aplicar deploy dados-pessoais-api-vi
oc create/replace -f deployment-dados-pessoais-api-v1.yaml

## Aplicar deploy endereco-api-vi
oc create/replace -f deployment-iptu-api-v1.yaml

## Delete all resource de portal-cidadao
oc delete all -l projeto=portal-cidadao

## Acessando o pod do kafka
oc rsh my-cluster-kafka-0 /bin/bash

### consumindo e pruduzindo eks sgn
./kafka-console-consumer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic=payment-topic

./kafka-console-producer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic=payment-topic


## Consumindo dados de um topico kafka
./kafka-console-consumer.sh --bootstrap-server=my-cluster-kafka-bootstrap.portal-cidadao.svc.cluster.local:9092 --topic=iptu-retorno-pagamento

./kafka-console-consumer.sh --bootstrap-server=debezium-cluster-kafka-bootstrap.debezium-example.svc:9092 --topic=debezium-example.inventory.orders

## -- para cadastrodb, o topic é separado em
## namespace.banco_de_dados.schema.table
./kafka-console-consumer.sh --bootstrap-server=debezium-cluster-kafka-bootstrap.atendimento.svc:9092 --topic=atendimento.cadastrodb.cadastrodb.owners


 ./kafka-console-consumer.sh --bootstrap-server=debezium-cluster-kafka-bootstrap.debezium-example.svc:9092 --topic=debezium-example.inventory.inventory.orders


./kafka-console-consumer.sh --bootstrap-server=debezium-cluster-kafka-bootstrap.debezium-example.svc:9092 --topic=debezium-example.inventory.inventory.orders


./kafka-console-consumer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sitram-intercon.svc.cluster.local:9092 --topic=processar-mdfe

{
"nuMdfe": "123XX",
"nfes":
[
  {"nuNfe": "AA11",
  "dtNfe": "2020-12-12",
  "vrNfe": 23.0
  },
  {"nuNfe": "BB22",
  "dtNfe": "2020-12-13",
  "vrNfe": 24.0
  },
]
}

{"nuMdfe": "123XX","nfes":[{"nuNfe": "AA11","dtNfe": "2020-12-12","vrNfe": 23.0},{"nuNfe": "BB22","dtNfe":"2020-12-13","vrNfe": 24.0},]}

## Enviando dados para um topico kafka
./kafka-console-producer.sh --bootstrap-server=my-cluster-kafka-bootstrap.portal-cidadao.svc.cluster.local:9092 --topic=iptu-retorno-pagamento

./kafka-console-producer.sh --bootstrap-server=localhost:9092 --topic=pje.public.tbprocesso

./kafka-console-producer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sitram-intercon.svc.cluster.local:9092 --topic=processar-mdfe


# debezium postgre a configuração abaixo tem que ser configurada no /var/lib/pgsql/data/postgresql.conf
wal_level = logical

# debezium habilitar o atrbuito before no payload para postgres
ALTER TABLE public.tbprocesso  REPLICA IDENTITY FULL;

# CamelK create secret
oc create sgeneric processar-alteracao-processo-props --from-file application.properties

oc create secret generic route-processar-mdfe-props --from-file application.properties

oc create secret generic route-processar-mdfe-props --from-file application.properties

# Camelk executar uma rota com secrete
kamel run --secret <nome-da-secrete> nome-da-classe-java --dev
###### Exemplo:
kamel run --secret processar-alteracao-processo-props  ProcessarAlteracaoProcesso.java --dev
###### Com dependencias
kamel run --secret processar-alteracao-processo-props -d mvn:com.google.code.gson:gson:2.10,org.postgresql:postgresql:42.6.0 ProcessarAlteracaoProcesso.java --dev

kamel run --secret route-processar-mdfe-props -d mvn:com.google.code.gson:gson:2.10,org.postgresql:postgresql:42.6.0 RouteProcessarMdfe.java --dev

kamel run --secret route-processar-mdfe-props -d mvn:com.google.code.gson:gson:2.10,org.postgresql:postgresql:42.6.0 RouteProcessarMdfe.java --dev


kamel run  --resource secret:route-processar-mdfe-props RouteProcessarMdfe.java -t knative-service.max-scale=1 --logs


## ProcessarPagamentoCartaoService
oc create secret generic processar-pagamento-cartao-props --from-file application.properties

### Excutar Kamen sem secret

kamel run  --resource secret:route-processar-mdfe-props RouteProcessarMdfe.java --dev

kamel run  --resource secret:route-processar-mdfe-props -d mvn:com.google.code.gson:gson:2.10,org.postgresql:postgresql:42.6.0 RouteProcessarMdfe.java --dev

kamel run chat-rest.groovy --trait knative-service.min-scale=1 --logs

curl -X PUT -H 'content-type: application/text' -d 'Olá pessoal' http://chat-rest.chat-xserver.svc.cluster.local/message/

chat-rest.chat-xserver.svc.cluster.local
 http://chat-channel-kn-channel.chat-xserver.svc.cluster.local

##### Step Create chat-server
###### 1 - Criar channel kafka
kn channel create chat-channel --type messaging.knative.dev:v1beta1:KafkaChannel
###### 2 - Criar Integration Platform
kamel install
###### 3 - Verificar se o integration platform existe
oc ge ip
###### 4 - Criar rota kamel chat-log.yaml
kamel run chat-log.yaml
###### 5 - Run WebSocket-Server
kamel run websocket-server.groovy --trait knative-service.enabled=true --trait knative-service.min-scale=1 --logs
##### 6 - Chamando WebSocket-Server com wscat
sudo wscat -c ws://websocket-server-chat-xserver.apps.cluster-rgswv.rgswv.sandbox753.opentlc.com/chat-server
##### 7 - Executando WebScket-Client  
kamel run chat-websocket.yaml --trait knative-service.min-scale=1 

#### 8 - Subir imagem chat
kn service create chat-webapp --image=quay.io/kharyam/chat-webapp:latest --scale=1..5 -e WEBSOCKET_URL=ws://websocket-server-chat-xserver.apps.cluster-rgswv.rgswv.sandbox753.opentlc.com:80/chat-server -e REST_URL=https://chat-rest-chat-xserver.apps.cluster-rgswv.rgswv.sandbox753.opentlc.com/message/



## Json Exemplo Pagamento
{"formaPagamento":"Pix","nsuPagamento":"00000000004","cpf":"00000000004","valorIptu":89.76,"numeroInscricao":"89898989","situacaoPagamento":"Processando Pagamento"}

{"formaPagamento":"Pix","nsuPagamento":"00000000007","cpf":"00000000007","valorIptu":99.00,"numeroInscricao":"INC00000007","situacaoPagamento":"Pagamento Pix Selecionado."}

{"formaPagamento":"Pix","nsuPagamento":"00000000002","cpf":"00000000002","valorIptu":84.00,"numeroInscricao":"INC00000002","situacaoPagamento":"Pagamento Enviado para Processamento."}

{"formaPagamento":"Pix","nsuPagamento":"00000000035","cpf":"00000000035","valorIptu":177.00,"numeroInscricao":"INC00000035","situacaoPagamento":"Pagamento Enviado para Processamento."}


curl "https://iptu-product-3scale-apicast-staging.apps.cluster-d64mj.d64mj.sandbox2653.opentlc.com:443/iptu/filtros?cpf=11111111111&user_key=portal-cidadao-key"

## Configuração cluster-hub
API_HUB=$(oc whoami --show-server)

API_PROD1="https://api.cluster-zvvvs.zvvvs.sandbox1535.opentlc.com:6443"
KUBEADMIN_PASSWORD_PROD1=xxxxxxx

ansible localhost -m lineinfile -a "path=${HOME}/.bashrc regexp='^export API_PROD1' line='export API_PROD1=${API_PROD1}'"

ansible localhost -m lineinfile -a "path=${HOME}/.bashrc regexp='^export KUBEADMIN_PASSWORD_PROD1' line='export KUBEADMIN_PASSWORD_PROD1=${KUBEADMIN_PASSWORD_PROD1}'"

oc config rename-context $(oc config current-context) prod1

ip http://149.56.238.185/

## Dica video zabbix
https://www.youtube.com/watch?v=EEzHdghvVLA

## Instalando repo zabbix para agente rhel9
rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/9/x86_64/zabbix-release-5.0-3.el9.noarch.rpm

## Instalando agente zabbix par rhell 9
sudo dnf install zabbix-agent2

## configurar zabbiz agent2
sudo vi /etc/zabbix/zabbix_agent2.conf

## Iniciar Zabbix
sudo service zabbix-agent2 start

## Colocando agente para iniciar 
sudo systemctl enable zabbix-agent2

## Criar um arquivo grande de 7 gb
fallocate -l 7.5G this.img

fallocate -l 7.1G /tmp/this.img

dd if=/dev/zero of=arquivo_teste.txt bs=1M count=9

## Verificar % uso em disco
df -h

# Executanto playbook ansible
ansible-playbook -i inventory.yml rotate-files.yml

# Adicionando configuração de host no AAP inventory
{
  "ansible_host": "13.48.24.39"
}

# Chamando para acionar job do AAP
curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer zwKpDWNkf8T4eBTg8CKpqGofoZrPfG" -d '{"credential": 7}' https://student1.8pxxv.example.opentlc.com//api/v2/job_templates/12/launch/


## custom login kc template
## copiando o template original
oc rsync keycloak-0:/opt/eap/themes/base/login/register.ftl .

## enviando template alterado
oc rsync  . keycloak-0:/opt/eap/themes/base/login --include=register.ftl --no-perms

## backup 3scale
3scale product export -f portal-cidadao-product.yaml  https://2ab8cf43f838f8cc4bdbbaa545287dd8d0f3e6dc540eab1bbe3489b810efe0ae@admin-admin.3scale-admin.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com Portal_Cidadao_Product


3scale product export -f portal-cidadao-product.yaml https://2ab8cf43f838f8cc4bdbbaa545287dd8d0f3e6dc540eab1bbe3489b810efe0ae@admin-admin.3scale-admin.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com Portal_Cidadao_Product

0d952cc17f30f7b8f2cd18ee99ceb450b7803f0775c75d30996aeaf56afa3baa

3scale product export -f portal-cidadao-product.yaml https://0d952cc17f30f7b8f2cd18ee99ceb450b7803f0775c75d30996aeaf56afa3baa@3scale-admin.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com/ Portal_Cidadao_Product

fN8kNh1D

3scale  product export -f portal-cidadao-product.json https://admin:2ab8cf43f838f8cc4bdbbaa545287dd8d0f3e6dc540eab1bbe3489b810efe0ae@3scale-admin.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com/ Portal_Cidadao_Product 

2ab8cf43f838f8cc4bdbbaa545287dd8d0f3e6dc540eab1bbe3489b810efe0ae


3scale remote add instance_a https://3scale-admin.apps.cluster-rwq8m.rwq8m.sandbox3105.opentlc.com

ghp_7eLFs0XLXIEjNVIBz04QWy7dwEwoPN2MJTp

# rbac para gitops system:cluster-admins, role:admin
oc adm policy add-cluster-role-to-user cluster-admin system:serviceaccount:openshift-gitops:openshift-gitops-argocd-application-controller 

oc policy add-role-to-user system:image-puller system:serviceaccount:atendimento:default --namespace=cicd -n atendimento

oc policy add-role-to-user system:image-puller system:serviceaccount:atendimento:default --namespace=cicd -n cicd

# postgres comandos
\l - lista banco de dados
\c - usa um banco deados, exmploe \c <db-name>
\d - lista tabelas, tem que primeiro está usando o banco com comando \c
\d+ - lista colunas tabela, example \d+ <table-name>

# keytools import certificado

keytool -import -trustcacerts  -noprompt -alias kcsogni-dev -file ca-bundle.crt

keytool -import -noprompt -trustcacerts -alias sogni-keycloak-dev -file "ca-bundle.crt" -keystore cacerts

# aws cli com eks comandos::
aws configure
aws eks update-kubeconfig --name <<nome do cluste eks>>

aws eks --region sa-east-1 update-kubeconfig --name sogni-cluster-01


## Gera o certificado par colocar na secret que o keycloak ira utilizaz
openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout auth-tls.key -out auth-tls.crt -subj "/CN=sa-east-1.elb.amazonaws.com/O=af27c840e9cee43b4ac15b97524d3b42-1424089969"

## create secret de certificado que o keycloak referencia
kubectl create secret -n keycloak tls auth-tls-secret --key auth-tls.key --cert auth-tls.crt

kubectl create secret -n sogni-dev tls sso-dev-tls-secret --key privkey.pem --cert cert.pem


kubectl create secret -n sogni-dev tls sso-dev-tls-secret --key /etc/letsencrypt/live/sso-dev.sogni.digital/privkey.pem --cert /etc/letsencrypt/live/sso-dev.sogni.digital/cert.pem


kubectl create secret -n sogni-dev tls sogni-dev-tls-secret --key privkey.pem --cert cert.pem


openssl s_client -connect af27c840e9cee43b4ac15b97524d3b42-1424089969.sa-east-1.elb.amazonaws.com:443 -showcerts 2>/dev/null </dev/null | awk '/^.*'"af27c840e9cee43b4ac15b97524d3b42-1424089969.sa-east-1.elb.amazonaws.com"'/,/-----END CERTIFICATE-----/{next;}/-----BEGIN/,/-----END CERTIFICATE-----/{print}' >>ca-bundle.crt

openssl s_client -connect sso-dev.sogni.digital:443 -showcerts 2>/dev/null </dev/null | awk '/^.*'"sso-dev.sogni.digital"'/,/-----END CERTIFICATE-----/{next;}/-----BEGIN/,/-----END CERTIFICATE-----/{print}' >>ca-bundle.crt


### adicionar apache solr
#### 1 adicionar o repo
helm repo add apache-solr https://solr.apache.org/charts
helm repo update


## Install Operator Solr por helm chart
helm install solr-operator apache-solr/solr-operator -n solr-stage

## Create instancias Solr 9.4 e zookeeper 
 helm install solr-stage \
  --set image.tag=9.4 \
  --set dataStorage.type="persistent" \
  --set dataStorage.capacity="7Gi" \
  --set dataStorage.persistent.reclaimPolicy="Retain" \
  --set zk.provided.storageType="persistent" \
  --set zk.privided.capacity="7Gi" \
  --set zk.provided.persistence.reclaimPolicy="Retain" \
  apache-solr/solr -n solr-stage

## pelo operator bitnami
helm repo add bitnami https://charts.bitnami.com/bitnami

helm install solr-stage \
--set persistence.enabled=true \
--set persistence.storageClass=efs-solr-sc \
--set persistence.existingClaim=data-solr-stage-0 \
--set replicaCount=1 \
--set persistence.size=2Gi \
--set zookeeper.replicaCount=1 \
--set zookeeper.persistence.persistence=true \
--set zookeeper.persistence.size=3Gi \
--set zookeeper.persistence.storageClass=efs-zookeeper-sc \
bitnami/solr -n solr-stage

--set authentication.adminPassword=admin,replicaCount=2,javaMem=512m,heap=512m \ 



### Port forward Solr UI
kubectl port-forward service/solrcloud-common 3000:80 -n sogni-dev

aws apigatewayv2 create-authorizer \
    --name sogni-kc \
    --api-id vsxj37um6c \
    --authorizer-type JWT \
    --identity-source '$request.header.Authorization' \
    --jwt-configuration Audience=audience,Issuer=https://af27c840e9cee43b4ac15b97524d3b42-1424089969.sa-east-1.elb.amazonaws.com/realms/sogni/.well-known/openid-configuration



cdk deploy --all \
  --parameters Service:jwtIssuer=https://af27c840e9cee43b4ac15b97524d3b42-1424089969.sa-east-1.elb.amazonaws.com/realms/sogni \
  --parameters Service:jwtAudience=account

KEYCLOAK_URL="https://af27c840e9cee43b4ac15b97524d3b42-1424089969.sa-east-1.elb.amazonaws.com/realms/sogni/protocol/openid-connect/token/"
CLIENT_ID="sogni-backend-api"
CLIENT_SECRET="1YgFFSNDvY5eVorz2T79gqbnBvWYq8VY"
USERNAME="joao.silva@gmail.com"
PASSWORD="1234"




curl -k -X POST ${KEYCLOAK_URL} -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=password" -d "client_id=${CLIENT_ID}" -d "client_secret=${CLIENT_SECRET}" -d "username=${USERNAME}" -d "password=${PASSWORD}" 

grant_type=password&client_id=sogni-backend-api&client_secret=1YgFFSNDvY5eVorz2T79gqbnBvWYq8VY&username=joao.silva@gmail.com&password=1234

## deploy quarkus em aws lambda
export LAMBDA_ROLE_ARN=arn:aws:iam::551038514168:role/sogni-lambda
sam deploy -t sam.jvm.yaml -g


 http://localhost:3000/solr/admin/collections?action=CREATE&name=&numShards=1&replicationFactor=1

 docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t quay.io/farid_ahmad/cidadao/iptu-api:1.0 .


 ./gradlew :account:account-infra:quarkusDev -x test -Djavax.net.ssl.trustStore=/Users/fahmad/Workspace/source/tekton/sognisport/certs/sso-dev.sogni.digital/truststore.jks -Djavax.net.ssl.trustStorePassword=changeit

 ## Login ecr aws registry
 login 

aws ecr get-login-password --region sa-east-1 | docker login --username AWS --password-stdin 551038514168.dkr.ecr.sa-east-1.amazonaws.com

docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t 551038514168.dkr.ecr.sa-east-1.amazonaws.com/sogni/account:1.0 .

docker push 551038514168.dkr.ecr.sa-east-1.amazonaws.com/account:1.0     


docker build --platform linux/amd64 -f ./src/main/docker/Dockerfile.jvm -t 551038514168.dkr.ecr.sa-east-1.amazonaws.com/sales-plan:1.0 .

docker push 551038514168.dkr.ecr.sa-east-1.amazonaws.com/sales-plan:1.0

docker push 551038514168.dkr.ecr.sa-east-1.amazonaws.com/custom-field:1.0

## instalar kafa no eks referencia 
https://redhat-developer-demos.github.io/kafka-tutorial/kafka-tutorial/1.0.x/07-kubernetes.html#strimzi

### instalar o strmimizi
kubectl apply -f 'https://strimzi.io/install/latest?namespace=sgn-dev' -n sgn-dev

kubectl get crds | grep kafka

## instla um cluster kafka
kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-ephemeral.yaml -n default

### consumindo e pruduzindo eks sgn
./kafka-console-consumer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic=sogni-sales-orders

./kafka-console-consumer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic=postgres.sogni.account_association


kafka-broker-api-versions.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092


./kafka-console-producer.sh --bootstrap-server=my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic=sogni-sales-orders

### Habilitando drivc csi no eks para colocar o pvc in bound

eksctl utils associate-iam-oidc-provider --region=sa-east-1 --cluster=sogni-cluster-01 --approve

eksctl create iamserviceaccount \
  --region sa-east-1 \
  --name ebs-csi-controller-sa \
  --namespace kube-system \
  --cluster sogni-cluster-01 \
  --attach-policy-arn arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy \
  --approve \
  --role-only \
  --role-name AmazonEKS_EBS_CSI_DriverRole


eksctl create addon --name aws-ebs-csi-driver --cluster sogni-cluster-01 --service-account-role-arn arn:aws:iam::$(aws sts get-caller-identity --query Account --output text):role/AmazonEKS_EBS_CSI_DriverRole --force


## criando bificado interando com route53
sudo certbot certonly --dns-route53 -d sso-dev.sogni.digital

# Gerar Certificado com certbot letencrypt
certbot certonly --manual --key-type rsa --preferred-challenges=http --config-dir ~/lets-encrypt --work-dir ~/lets-encrypt --logs-dir ~/lets-encrypt

### Gerar Certificado com certbot letencrypt manual com o dominio especifico
sudo certbot certonly --manual --preferred-challenges dns -d sso-dev.sogni.digita


# create seceret tls
kubectl create secret tls cert-https-app-stage.sogni.digital --cert=cert.pem --key=privkey.pem -n sogni-dev


kubectl create secret tls sso-dev-tls-secret --cert=cert.pem --key=privkey.pem -n sogni-dev

## Nginx Acesso Privilegiado
quay.io/farid_ahmad/cidadao/nginx-h:1.0
oc adm policy add-scc-to-user anyuid -z default

## eterpad altamente vulneravel
podman pull quay.io/redhattraining/etherpad


./kafka-console-consumer.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic postgres.sogni.account_association --from-beginning

## comando de configuração do topic do kafka-connector
./kafka-configs.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --entity-type topics --entity-name connect-offsets --alter --add-config cleanup.policy=compact

./kafka-topics.sh --list --bootstrap-server localhost:9092

./kafka-topics.sh --delete --topic postgres.sogni.account --bootstrap-server  my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092

./kafka-topics.sh --delete --topic postgres.sogni.account_association --bootstrap-server  my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092

## Executar esse compando para balancear e retira os erros Error: NOT_ENOUGH_REPLICAS
./kafka-configs.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --entity-type topics --entity-name connect-offsets --alter --add-config cleanup.policy=compact


./kafka-configs.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --entity-type topics --entity-name connect-configs --alter --add-config cleanup.policy=compact

./kafka-configs.sh --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --entity-type topics --entity-name connect-status --alter --add-config cleanup.policy=compact


./kafka-configs.sh --alter --entity-type topics --entity-name connect-status --add-config min.insync.replicas=3 --bootstrap-server  my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092

./kafka-configs.sh --alter --entity-type topics --entity-name connect-configs --add-config min.insync.replicas=3 --bootstrap-server  my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092

./kafka-configs.sh --alter --entity-type topics --entity-name connect-offsets --add-config min.insync.replicas=3 --bootstrap-server  my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092


./kafka-configs.sh --alter --entity-type topics --entity-name postgres.sogni.accoutns_association --add-config retention.ms=1 --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092


##listar topico
./kafka-topics.sh --list --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092

## excluir topico
./kafka-topics.sh --delete --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic postgres.sogni.account_association

## excluir topico
./kafka-topics.sh --delete --bootstrap-server my-cluster-kafka-bootstrap.sogni-dev.svc.cluster.local:9092 --topic postgres.sogni.business_account