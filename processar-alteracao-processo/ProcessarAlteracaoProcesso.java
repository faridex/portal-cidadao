import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.model.dataformat.JsonLibrary;
import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
import java.util.ArrayList;
import org.apache.camel.component.mysql.MySqlComponent;



import com.google.gson.Gson;

public class RouteProcessarMdfe extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        log.info("About to start route: Kafka -> Log ");
        Gson gson = new Gson();
       

        JdbcComponent jdbc = new JdbcComponent();
        jdbc.setDriverClassName("com.mysql.cj.jdbc.Driver");
        jdbc.setUrl("jdbc:mysql://149.56.238.185:3306/pje_extracao");
        jdbc.setUsername("admin");
        jdbc.setPassword("senha");
        getContext().addComponent("jdbc", jdbc);
       
        from("kafka:{{consumer.topic}}")
            .routeId("FromKafka2Log")
            .log("Alteracao Recebida ${body}")
            .process(exchange ->{
                String json = exchange.getIn().getBody(String.class);
                Root root = gson.fromJson(json, Root.class);
                Payload payload = null;
                if(root != null){
                    System.out.println("ROOT CARREGADO ");
                    if(root.payload.before != null && root.payload.after != null){
                        System.out.println("ALTERACAO DE REGISTRO NOVO ");   
                        System.out.println("PROCESSO ALTERADO::  "+root.payload.before.co_processo );   
                    }else if(root.payload.before == null && root.payload.after != null){
                        System.out.println("NOVO REGISTRO ");    
                        System.out.println("PROCESSO INCLUIDO::  "+root.payload.after.co_processo ); 
                    }
                }
                    
                else
                    System.out.println("PAYLOAD NAO CARREGADO ");

            });
           
            //.to("direct:processRootBean");

        from("direct:processRootBean")    
            //.bean(this,"processRootBean")
            .log("Pojo Recebido ${body}");
    }

   

    public void processRootBean(Exchange exchange) {
        Root root = exchange.getIn().getBody(Root.class);
        System.out.println("BeaN::: coprocesso:"+root.payload.after.co_processo);
       // exchange.getIn().setBody("BeaN::: coprocesso:"+root.payload.after.co_processo);
    }
   
    public void processPayload(Exchange exchange) {
        String payload = exchange.getIn().getBody(String.class);
       // Gson gson = new Gson();
       // Root root = gson.fromJson(payload, Root.class);
        //exchange.getIn().setBody("alterado:["+root.payload.after.co_processo+"]");
    }    

    public static class After{
        public String co_processo;
        public int dt_processo;
    }
    
    public static class Before{
        public String co_processo;
        public int dt_processo;
    }
    
    public static class Field{
        public String type;
        public ArrayList<Field> fields;
        public boolean optional;
        public String name;
        public String field;
        public int version;
        public Parameters parameters;
        @JsonProperty("default") 
        public String mydefault;
    }
    
    public static class Parameters{
        public String allowed;
    }
    
    public static class Payload{
        public Before before;
        public After after;
        public Source source;
        public String op;
        public long ts_ms;
        public Object transaction;
    }
    
    public static class Root{
        public Schema schema;
        public Payload payload;
    }
    
    public static class Schema{
        public String type;
        public ArrayList<Field> fields;
        public boolean optional;
        public String name;
    }
    
    public static class Source{
        public String version;
        public String connector;
        public String name;
        public long ts_ms;
        public String snapshot;
        public String db;
        public String schema;
        public String table;
        public int txId;
        public int lsn;
        public Object xmin;
    }


    
}
