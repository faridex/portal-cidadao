package com.cidadao.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.api.DadosPessoaisApi;
import com.cidadao.entity.DadosPessoais;
import com.cidadao.entity.Pagamento;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;

@ApplicationScoped
public class PagamentoService {
    Logger logger = Logger.getLogger(PagamentoService.class.getName());

    private static Map<String,List<Pagamento>> respostaPagamento = new HashMap<String,List<Pagamento>>();

    @RestClient
    DadosPessoaisApi dadosPessoaisApi;

    @Inject
    @Channel("iptu-requisicao-pagamento")
    Emitter<String> sendRequestPaymentEmitter;

    @Inject
    MeterRegistry registry;

   
    public Pagamento doSendRequestPayment(Pagamento pagamento) {
        DadosPessoais dadosPessoais = dadosPessoaisApi.doGetByFilter(pagamento.cpf);
        String jsonWithJsonb = JsonbBuilder.create().toJson(pagamento);
        logger.info("Colocando mensagem no topico iptu-requisicao-pagamento "+ jsonWithJsonb);
        CompletionStage<Void> ack = sendRequestPaymentEmitter.send(jsonWithJsonb);
        pagamento.situacaoPagamento="Enviado para Processamento";
        return pagamento;
    }

    
    @Incoming("iptu-retorno-pagamento")
    public void doListenReturnPayment(String payload){
        logger.info("Mensagem Recebida do topico iptu-retorno-pagamento >> "+ payload +" <<");
        Jsonb jsonb = JsonbBuilder.create();
        logger.info("Convertendo payload");
        Pagamento pagamento = jsonb.fromJson(payload, Pagamento.class);
        if(!respostaPagamento.containsKey(pagamento.cpf)){
            respostaPagamento.put(pagamento.cpf, new ArrayList<Pagamento>());
        }
        logger.info("Armazenamento de Objeto com status ::: "+pagamento.situacaoPagamento);
        respostaPagamento.get(pagamento.cpf).add(pagamento);
        if(pagamento != null && pagamento.situacaoPagamento.equals("Pagamento Validado.") ){
            if(pagamento.formaPagamento.equals("Pix")){
                registry.counter("pagamento.pix", Tags.of("valor_total_pix", pagamento.formaPagamento)).increment();
            }else{
                registry.counter("pagamento.cartao_credito", Tags.of("quantidade_cartao_credito", pagamento.formaPagamento)).increment();
            }
        }
        

        

    }

    public List<Pagamento> doGetPaymentForFilter(String cpf){
        return this.respostaPagamento.get(cpf);
    }

    
}
