package com.cidadao.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.cidadao.entity.DadosPessoais;

@Path("/dados-pessoais")
@RegisterRestClient
@ApplicationScoped
public interface DadosPessoaisApi {

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public DadosPessoais doGetByFilter(
            @QueryParam("cpf") String cpf);
}
