package com.cidadao;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cidadao.entity.Pagamento;
import com.cidadao.service.PagamentoService;



@Path("/pagamento")
public class PagamentoResource {
    Logger logger = Logger.getLogger(PagamentoResource.class.getName());
    
    @Inject
    PagamentoService pagamentoService;

    @POST
    public Pagamento doSendPayment(Pagamento pagamento){
        Pagamento _pagamento = pagamentoService.doSendRequestPayment(pagamento);
        return _pagamento;
    }
    
    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pagamento> doGetPaymentForFilter(@QueryParam("cpf") String cpf){
        return pagamentoService.doGetPaymentForFilter(cpf);
    }
   
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String doTestGet(){
        return "is work";
    }
}