import { APP_INITIALIZER,NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { QRCodeModule } from 'angularx-qrcode';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IptuComponent } from './iptu/iptu.component';
import { HomeComponent } from './home/home.component';
import { BoletimComponent } from './boletim/boletim.component';
import { FormsModule } from '@angular/forms';
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
      keycloak.init({
        config: {
          url: 'https://keycloak-sso.apps.cluster-sv5db.sv5db.sandbox2461.opentlc.com/auth',
          realm: 'portal-cidadao',
          clientId: 'frontend',
        },
        initOptions: {
            onLoad: 'login-required'
        },
        bearerExcludedUrls: ['/assets']
      });
}

@NgModule({
  declarations: [
    AppComponent,
    IptuComponent,
    BoletimComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    KeycloakAngularModule,
    HttpClientModule,
    AppRoutingModule,
    QRCodeModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
  },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
