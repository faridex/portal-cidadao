import { Component, Input } from '@angular/core';
import { IptuService } from './iptu.service';
import { Iptu } from '../model/iptu';
import { DadosPessoais } from '../model/dados-pessoais';
import { Endereco } from '../model/endereco';
import { Pagamento } from '../model/pagamento';
import { WebSocketService } from '../web-socket/websocket.service';
import { interval } from "rxjs/internal/observable/interval";
import { switchMap, takeWhile } from 'rxjs';



@Component({
  selector: 'app-iptu',
  templateUrl: './iptu.component.html',
  styleUrls: ['./iptu.component.css']
})
export class IptuComponent {
  public iptus: Iptu[] = [];
  public iptu!: Iptu;
  public iptuSelecionado!: Iptu;
  public dadosPessoais!: DadosPessoais;
  public endereco!: Endereco;
  public formaPagamentoSelecionada!: string;
  public tpFormaPagamentoSelecionada!: string;
  nuInscricaoImovel = null;
  nuCpfCnpj = null;
  public messageError = '';
  public retornoPagamento: Pagamento[] = [];
  public isHideModalPagamento:string = 'false';
  public tamanhoRetorno = 0;
  public emProcessamento = 'false';
  public emProcessamentoConsulta = 'false';
  public pixUrlQrCode = '';
  
   

  constructor(private iptuService: IptuService
        ,public webSocketService : WebSocketService) { 

          this.webSocketService.connect("ws://pagamento-api-portal-cidadao.apps.cluster-d6chv.d6chv.sandbox2781.opentlc.com/iptu-pagamento/123")
          this.retornoPagamento = this.webSocketService.receivedData;
          
          
        }

  fecharModalPagamento(){
    this.isHideModalPagamento='false';
  }
  prepareRealizarPagamentoCartaoCredito(iptu: Iptu){
    this.isHideModalPagamento='true';
    this.iptuSelecionado = iptu;
    this.tpFormaPagamentoSelecionada='CC';
    this.messageError='';
  }  
  
  prepareRealizarPagamentoPix(iptu: Iptu){
    this.isHideModalPagamento='true';
    this.emProcessamento = 'true'
    this.iptuSelecionado = iptu
    this.tpFormaPagamentoSelecionada='PX';
    const b64 = "SGVsbG8sIFdvcmxkIQ==";
    var _valorDecoder64 = btoa(iptu.valorIptu);
    this.pixUrlQrCode =  'http://iptu-api-portal-cidadao.apps.cluster-sv5db.sv5db.sandbox2461.opentlc.com/iptu/confirmar-pix?inc='+
    iptu.cpf+'&vic='+_valorDecoder64;
    this.doGetPaymentByFilter();
   
  } 

  prepareRealizarPagamentoBoleto(iptu: Iptu){
    this.isHideModalPagamento='true';
    this.iptuSelecionado = iptu;
    this.tpFormaPagamentoSelecionada='BO';
  }
  
  realizarPagamentoCartaoCredito() {
    this.emProcessamento = 'true'
    let pagamento = new Pagamento();
    pagamento.numeroInscricao = this.iptuSelecionado.numeroInscricao
    pagamento.cpf = this.iptuSelecionado.cpf
    pagamento.formaPagamento='Cartao de Crédito';
    this.iptuService.realizarPagamentoCartaoCredito(pagamento)
    .subscribe({
      next: (data) => {
        let pagamento!: Pagamento;
        pagamento = data;
        this.iptu.situacaoPagamento = pagamento.situacaoPagamento
        this.doGetPaymentByFilter()
      },
      error: (e) => {
        this.emProcessamento='false';
        if(e.error == 'No Mapping Rule matched'){
          this.messageError='Falha ao tentar realizar pagamento por Cartão. Serviço não foi alcançado !!';
        }
        console.log(e);
      }
    });
   
  }

  

  realizarPagamentoPix() {
    this.emProcessamento = 'true'
    let pagamento = new Pagamento();
    pagamento.numeroInscricao = this.iptuSelecionado.numeroInscricao
    pagamento.cpf = this.iptuSelecionado.cpf
    pagamento.formaPagamento='Pix';
    this.iptuService.realizarPagamentoPix(pagamento)
    .subscribe({
      next: (data) => {
        let pagamento!: Pagamento;
        pagamento = data;
        this.iptu.situacaoPagamento = pagamento.situacaoPagamento
        this.doGetPaymentByFilter()
      },
      error: (e) => {
        console.log(e);
      }
    });
   
  }
  
  doGetPaymentByFilter() {
    this.iptuService.getPaymentoByFilter(this.iptuSelecionado.cpf)
      .subscribe({
        next: (data) => {
          this.retornoPagamento = data;
          if(this.retornoPagamento != null && this.tpFormaPagamentoSelecionada=='CC'){
              this.tamanhoRetorno = this.retornoPagamento.length
              if(this.tamanhoRetorno < 4){
                this.doGetPaymentByFilter()
              }else{
                this.emProcessamento = 'false'
              }
          }else if( this.tpFormaPagamentoSelecionada=='PX'){
            if(this.retornoPagamento == null || this.retornoPagamento.length < 4){
              this.doGetPaymentByFilter()
            }else{
              this.emProcessamento = 'false'
            }
        }

        },
        error: (e) => {
          console.log(e);
        }
      });
  }

  exibirBtnRealizarPagamento(){
    let situacao = '';
    for (let i = 0; i < this.retornoPagamento.length; i++) {
      situacao = this.retornoPagamento[i].situacaoPagamento;
    }

    for (let i = 0; i < this.iptus.length; i++) {
      this.iptus[i].situacaoPagamento = situacao;
     }

    if (situacao == 'Pagamento Validado.'){
      this.fecharModal();
      return false;
    } 
    if(this.tpFormaPagamentoSelecionada == 'CC')
      return true;
    else
      return false;  
    
  }

  fecharModal(){
    this.isHideModalPagamento='false';
  }

  consultarRegistros() {
    this.emProcessamentoConsulta = 'true';
    console.log('cpf proprietario:: ' + this.nuCpfCnpj);
    this.retornoPagamento = [];
    this.iptuService.get(this.nuCpfCnpj)
      .subscribe({
        next: (data) => {
          this.iptus = data;
          this.iptu = this.iptus[0];
          console.log(data);
          this.emProcessamentoConsulta = 'false';
        },
        error: (e) => {
          console.log(e);
          this.emProcessamentoConsulta = 'false';
        }
      });
  }
}