package com.cidadao;

import java.util.List;

import com.cidadao.entity.Cidadao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/common-api")
@ApplicationScoped
public class CidadaoResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Cidadao> hello() {
        return Cidadao.listAll();
    }

    

}
