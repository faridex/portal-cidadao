package com.cidadao;

import org.eclipse.microprofile.jwt.JsonWebToken;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/common-api")
@ApplicationScoped
public class CommonResource {

    @Inject
    SecurityIdentity securityIdentity;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"roles/account:customer-account:read"})
    public String doGet() {
        JsonWebToken jwt = (JsonWebToken) securityIdentity.getPrincipal();
        String useString = "";
        if(jwt != null){
            useString = jwt.getName();
        }
        return "Get Executado "+ useString;
    }

    

}
