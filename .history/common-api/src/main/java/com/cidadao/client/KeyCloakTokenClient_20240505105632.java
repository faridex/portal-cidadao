package com.cidadao.client;


import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.core.MediaType;

@RegisterRestClient
@ApplicationScoped
public interface KeyCloakTokenClient {

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    String getToken(@FormParam("grant_type") String grantType,
                    @FormParam("client_id") String clientId,
                    @FormParam("client_secret") String clientSecret);
}