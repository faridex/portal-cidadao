package com.cidadao;

import java.security.Principal;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.SecurityContext;

public class CustomSecurityContext implements SecurityContext {

    private final SecurityContext delegate;
    
    @Inject
    SecurityIdentity securityIdentity;

    public CustomSecurityContext(SecurityContext delegate) {
        this.delegate = delegate;
    }

    @Override
    public Principal getUserPrincipal() {
        return delegate.getUserPrincipal();
    }

    @Override
    public boolean isUserInRole(String role) {
        // Lógica para buscar papéis dinamicamente em resource_access
        // Substitua isso pela sua lógica de busca de papéis
        // Exemplo:
        // return verificarPapelDinamicamente(role);
        return false; // Implemente sua lógica real aqui
    }

    @Override
    public boolean isSecure() {
        return delegate.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return delegate.getAuthenticationScheme();
    }
}
