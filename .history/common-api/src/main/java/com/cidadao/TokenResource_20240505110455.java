package com.cidadao;

import org.eclipse.microprofile.jwt.JsonWebToken;

import com.cidadao.client.KeyCloakTokenClient;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;

@Path("/exchange-token")
@ApplicationScoped
public class TokenResource {

    @Inject
    SecurityIdentity securityIdentity;

    @Inject
    SecurityContext securityContext;

    @Inject
    KeyCloakTokenClient keyCloakTokenClient;

    @POST
    @Path("/{account}")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"sogni-exchange-token-br"})
    public String doExchangeToken(@PathParam("account") String account) {
        System.out.println("Realizando o Exchange-Token para o client "+account);
        String token = keyCloakTokenClient.getToken("client_credentials",account, "aZPnuVdhoqfJgBl5GTnpbPws4vp28hEx");
        System.out.println("Token Gerando..");
        return token;
        /* 

        JsonWebToken jwt = (JsonWebToken) securityIdentity.getPrincipal();
        String useString = "";
        if(jwt != null){
            useString = jwt.getName();
        }
        return "Get Executado "+ useString;
        */
    }

    

}
