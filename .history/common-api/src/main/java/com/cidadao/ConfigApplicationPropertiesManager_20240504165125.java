package com.cidadao;


import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ConfigApplicationPropertiesManager {

    @ConfigProperty(name = "quarkus.oidc.roles.role-claim-path")
    String roleClaimPath;

    public void setRoleClaimPath(String path) {
        this.roleClaimPath = path;
    }

    public String getRoleClaimPath() {
        return roleClaimPath;
    }
}
