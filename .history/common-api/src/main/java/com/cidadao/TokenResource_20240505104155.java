package com.cidadao;

import org.eclipse.microprofile.jwt.JsonWebToken;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;

@Path("/common-api")
@ApplicationScoped
public class CommonResource {

    @Inject
    SecurityIdentity securityIdentity;

    @Inject
    SecurityContext securityContext;

    @GET
    @Path("/{account}")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"account:customer-account:read"})
    public String doGet(@PathParam("account") String account) {
        System.out.println("doGet Acionado para o acount "+account);
        if(securityContext.isUserInRole("account:customer-account:write")){
            System.out.println("tem a role ");
        }else{
            System.out.println("não tem a role");
        }
        JsonWebToken jwt = (JsonWebToken) securityIdentity.getPrincipal();
        String useString = "";
        if(jwt != null){
            useString = jwt.getName();
        }
        return "Get Executado "+ useString;
    }

    

}
