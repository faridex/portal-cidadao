package com.cidadao;

import java.util.List;


import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/common-api")
@ApplicationScoped
public class CommonResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String hello() {
        return "Get Executado";
    }

    

}
