package com.cidadao;

import java.io.StringReader;
import java.security.Principal;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.microprofile.jwt.JsonWebToken;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.JsonValue;
import jakarta.ws.rs.core.SecurityContext;

public class CustomSecurityContext implements SecurityContext {

    private final SecurityContext delegate;
    private final SecurityIdentity securityIdentity;
    private final String accountPath; 
    public CustomSecurityContext(SecurityContext delegate,SecurityIdentity securityIdentity, String accountPath) {
        System.out.println("construtor CustomSecurity");
        this.delegate = delegate;
        this.securityIdentity = securityIdentity;
        this.accountPath = accountPath;
    }

    @Override
    public Principal getUserPrincipal() {
        return delegate.getUserPrincipal();
    }

    @Override
    public boolean isUserInRole(String role) {
        boolean isHaveRole=false;
        System.out.println("Role para verificar :: "+role);
        JsonWebToken jwt = (JsonWebToken) securityIdentity.getPrincipal();
        
        if(jwt != null){
            System.out.println(">>>> é uma instancia de jwtWebToken");
            System.out.println(jwt.getRawToken());
            JsonObject resourceAccess = parseJwtClaims(jwt.getRawToken()).getJsonObject("resource_access");
            JsonObject resource = resourceAccess.getJsonObject(accountPath);
            if (resource != null) {
                System.out.println("achou o resource "+resource.toString());
                JsonArray roles = resource.getJsonArray("roles");
                System.out.println("qtd role "+roles.size());
                for (JsonValue _valueRole : roles) {
                    
                    System.out.println("Role :: "+role + " matches "+_valueRole + " -> "+_valueRole.toString().equals(role.toString()));
                    if (_valueRole.toString().matches(role)) {
                        System.out.println(">>> role encontrada "+role);  
                        isHaveRole = true;  
                        break;
                    }
                }
                return isHaveRole;
            }
        }
       
        
        return false;
    }

    @Override
    public boolean isSecure() {
        return delegate.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return delegate.getAuthenticationScheme();
    }

    private JsonObject parseJwtClaims(String token) {
        // Extrai os claims do token JWT
        // Esta implementação depende da biblioteca que você está usando para manipulação de JWT
        // Aqui, estou assumindo que você está usando alguma biblioteca compatível com JSON
        String[] tokenParts = token.split("\\.");
        String claimsString = new String(Base64.getUrlDecoder().decode(tokenParts[1]));
        JsonReader reader = Json.createReader(new StringReader(claimsString));
        return reader.readObject();
    }
}
