package com.cidadao;

public class JwtCheckResourceAccessRole implements TokenSecurityCheck {

    @Inject
    SecurityIdentityAssociation securityIdentityAssociation;

    @Override
    public void apply(@Context HttpHeaders headers) {
        try {
            String token = headers.getHeaderString("Authorization").substring("Bearer ".length());
            DefaultJWTCallerPrincipal jwtPrincipal = (DefaultJWTCallerPrincipal) JWTCallerPrincipalFactory.instance().parse(token);
            JsonObject resourceAccess = jwtPrincipal.getClaim("resource_access");
            // Extraia as roles do resource_access e adicione ao SecurityIdentity
            // Por exemplo:
            String[] roles = resourceAccess.getJsonObject("cruzeiro-resource-client-br").getJsonArray("roles").getString(0).split(":");
            securityIdentityAssociation.setIdentity(new SecurityIdentity() {
                @Override
                public boolean isAnonymous() {
                    return false;
                }

                @Override
                public boolean hasRole(String role) {
                    for (String r : roles) {
                        if (r.equals(role)) {
                            return true;
                        }
                    }
                    return false;
                }

                // Implemente outros métodos da interface SecurityIdentity conforme necessário
            });
        } catch (ParseException e) {
            throw new AuthenticationFailedException("Erro ao analisar o token JWT", e);
        }
    }
