package com.cidadao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@ApplicationScoped
public class RoleClaimPathFilter implements ContainerRequestFilter {

    @Inject
    ConfigApplicationPropertiesManager configManager; // Sua classe para gerenciar a configuração

    @Context
    UriInfo uriInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        // Verifica se a requisição é para um endpoint específico onde você deseja alterar a propriedade
        if (uriInfo.getPath().startsWith("/example/change-role-claim-path")) {
            // Defina o novo valor para quarkus.oidc.roles.role-claim-path
            configManager.setRoleClaimPath("novo/caminho/da/claim-de-papel");
        }
    }
}
