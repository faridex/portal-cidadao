package com.cidadao;

import java.io.StringReader;
import java.security.Principal;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.microprofile.jwt.JsonWebToken;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.ws.rs.core.SecurityContext;

public class CustomSecurityContext implements SecurityContext {

    private final SecurityContext delegate;
    private final SecurityIdentity securityIdentity;
    public CustomSecurityContext(SecurityContext delegate,SecurityIdentity securityIdentity) {
        System.out.println("construtor CustomSecurity");
        this.delegate = delegate;
        this.securityIdentity = securityIdentity;
    }

    @Override
    public Principal getUserPrincipal() {
        return delegate.getUserPrincipal();
    }

    @Override
    public boolean isUserInRole(String role) {
        System.out.println("Role para verificar :: "+role);
        JsonWebToken jwt = (JsonWebToken) securityIdentity.getPrincipal();
        
        if(jwt != null){
            System.out.println(">>>> é uma instancia de jwtWebToken");
            System.out.println(jwt.getRawToken());
            JsonObject resourceAccess = parseJwtClaims(jwt.getRawToken()).getJsonObject("resource_access");
            JsonObject cruzeiroResource = resourceAccess.getJsonObject("cruzeiro-resource-client-br");
            if (cruzeiroResource != null) {
                
                System.out.println("achou o cruzeiro "+cruzeiroResource.toString());
                String[] roles = cruzeiroResource.getJsonArray("roles").getString(0).split(":");
                System.out.println("size roles "+ roles.length);
                for(String _role: roles){
                    System.out.println(">>> "+_role);
                }
                Set<String> roleSet = new HashSet<>(Arrays.asList(roles));
                return roleSet.contains(role);
            }
        }
        
        return false;
    }

    @Override
    public boolean isSecure() {
        return delegate.isSecure();
    }

    @Override
    public String getAuthenticationScheme() {
        return delegate.getAuthenticationScheme();
    }

    private JsonObject parseJwtClaims(String token) {
        // Extrai os claims do token JWT
        // Esta implementação depende da biblioteca que você está usando para manipulação de JWT
        // Aqui, estou assumindo que você está usando alguma biblioteca compatível com JSON
        String[] tokenParts = token.split("\\.");
        String claimsString = new String(Base64.getUrlDecoder().decode(tokenParts[1]));
        JsonReader reader = Json.createReader(new StringReader(claimsString));
        return reader.readObject();
    }
}
