package com.cidadao;

import jakarta.annotation.Priority;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.SecurityContext;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.ext.Provider;

@ApplicationScoped
@Priority(Priorities.AUTHENTICATION - 1)
@Provider
public class CustomSecurityContextProvider implements ContainerRequestFilter {

    @Context
    UriInfo uriInfo;
    
    @Context
    private SecurityContext securityContext;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        System.out.println("Path:::::::: "+ uriInfo.getPath());
        //SecurityContext currentContext = requestContext.getSecurityContext();
        CustomSecurityContext customContext = new CustomSecurityContext(securityContext);
        requestContext.setSecurityContext(customContext);
        
        
    }
}
