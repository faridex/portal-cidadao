package com.cidadao;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.api.HorarioOnibusApi;
import com.cidadao.api.HorarioOnibusApi.HorarioLinha;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;


@Path("/mobilidade-api")
@ApplicationScoped
public class MobilidadeResource {

    @RestClient
    HorarioOnibusApi horarioOnibusApi;

    @Inject
    @ConfigProperty(name = "url.linhas")
    String baseUrl;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonArray listarLinhas() {
        InputStream inputStream = getClass().getResourceAsStream("/linhas.json");

        JsonReader reader = Json.createReader(inputStream);
        JsonArray dataArray = reader.readArray();
        reader.close();

        return dataArray;
    }

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public String getDataByQuery(
        @QueryParam("code") String code,
        @QueryParam("data") String data){
            //List<HorarioLinha> horarios = linhaOnibusApi.doGetByFilter(code, data);
            /* 
            String url = baseUrl + "/api/linhas/" + code + "?data=" + data;
            System.out.println(">>> "+url);
            URL _baseURL;
            try {
                _baseURL = new URL(url);
            } catch (Exception e) {
                throw new RuntimeException("Erro ao construir a URL base: " + e.getMessage(), e);
            }
            LinhaOnibusApi client = RestClientBuilder.newBuilder()
                .baseUrl(_baseURL)
                .build(LinhaOnibusApi.class);
            List<HorarioLinha> horarios = client.doGetByFilter();
            */
           String retorno = horarioOnibusApi.doGetByFilter(code, data);
            System.out.println(">>>> "+ retorno);
            return retorno;
    }
    
    
    
}
