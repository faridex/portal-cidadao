package com.cidadao.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;



@Path("/")
@RegisterRestClient
@ApplicationScoped
public interface HorarioDisponivelApi {

    @GET
    public String doGetHorariosDisponiveis();
}
