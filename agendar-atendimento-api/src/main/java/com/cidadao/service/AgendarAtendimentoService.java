package com.cidadao.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import com.cidadao.api.HorarioDisponivelApi;
import com.cidadao.entity.PostoAtendimento;

@ApplicationScoped
public class AgendarAtendimentoService {
    Logger logger = Logger.getLogger(AgendarAtendimentoService.class.getName());

    @RestClient
    HorarioDisponivelApi horarioDisponivelApi;

    public List<PostoAtendimento> doGetByFilter(String postoAtendimento) {
        PostoAtendimento _postoAtendimento = new PostoAtendimento();
        String horarioAtendimento = horarioDisponivelApi.doGetHorariosDisponiveis();
        
        List<PostoAtendimento> _postos = new ArrayList<PostoAtendimento>();
        _postos.add(_postoAtendimento);
        return _postos;
    }

    
}
