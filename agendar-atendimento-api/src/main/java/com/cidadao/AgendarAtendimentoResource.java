package com.cidadao;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cidadao.entity.PostoAtendimento;
import com.cidadao.service.AgendarAtendimentoService;



@Path("/agendar-atendimento")
public class AgendarAtendimentoResource {
    Logger logger = Logger.getLogger(AgendarAtendimentoResource.class.getName());
    @Inject
    AgendarAtendimentoService agendarAtendimentoService;

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGetByFilter(
        @QueryParam("posto-atendimento") String postoAtendimento
    ) 
    {
        logger.info("Recebendo Requiscao para o posto de Atendimento ["+postoAtendimento+"]");
        List<PostoAtendimento> postos = agendarAtendimentoService.doGetByFilter(postoAtendimento);
        return Response.ok(postos).build();
    }
        
        
   
   
}