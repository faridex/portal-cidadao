package com.example.websocketdemo.controller;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import com.example.websocketdemo.model.ChatMessage;

/**
 * Created by rajeevkumarsingh on 24/07/17.
 */
@Controller
public class ChatController {

    
    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        mountResponse(chatMessage);
        return chatMessage;
    }

    private void mountResponse(ChatMessage chatMessage){
        if(chatMessage.getContent().toUpperCase().indexOf("OLÁ") >-1){
            chatMessage.setResponseContent("Olá "+chatMessage.getSender() +" digite o que deseja.");
            chatMessage.setResponseSender("Atendimento Virtual");
        }else if(chatMessage.getContent().toUpperCase().indexOf("ENVIANDO") >-1){
            try{Thread.sleep(4000);}catch(Exception e){};
            chatMessage.setResponseContent("Arquivo Recebido !! Estamos Analisando o conteúdo...");
            chatMessage.setResponseSender("Atendimento Virtual");
            chatMessage.setType(chatMessage.getType().AGUARDE);
        }else if(chatMessage.getContent().toUpperCase().indexOf("ISWAIT") >-1){
             try{Thread.sleep(3000);}catch(Exception e){};
             File fileBig = new File("/tmp/this.img");
             while(fileBig.exists()){
                try{Thread.sleep(3000);}catch(Exception e){};
             }
            chatMessage.setContent("Arquivo foi Validado com Sucesso :) ");
            chatMessage.setSender("Atendimento Virtual");
            chatMessage.setType(chatMessage.getType().VALIDADO);
        }else if(chatMessage.getContent().toUpperCase().indexOf("ENCERRAR") >-1){
            chatMessage.setResponseContent("OK !! Encerrando o Atendimento.");
            chatMessage.setResponseSender("Atendimento Virtual");
            chatMessage.setType(chatMessage.getType().ENCERRAR);
        }else if(chatMessage.getContent().toUpperCase().indexOf("ARQUIVO") >-1
        || chatMessage.getContent().toUpperCase().indexOf("AQUIVO") >-1){
            chatMessage.setResponseContent("Tudo bem... você já pode enviar o arquivo, estamos prontos para o receber !!");
            chatMessage.setResponseSender("Atendimento Virtual");
            chatMessage.setType(chatMessage.getType().ARQUIVO);
        }
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

}
