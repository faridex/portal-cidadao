package com.example.websocketdemo.model;

/**
 * Created by rajeevkumarsingh on 24/07/17.
 */
public class ChatMessage {
    private MessageType type;
    private String content;
    private String sender;

    private String responseContent;
    private String responseSender;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE,
        ARQUIVO,
        AGUARDE,
        VALIDADO,
        ENCERRAR
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

     public String getResponseContent() {
        return responseContent;
    }

    public void setResponseContent(String responseContent) {
        this.responseContent = responseContent;
    }

    public String getResponseSender() {
        return responseSender;
    }

    public void setResponseSender(String responseSender) {
        this.responseSender = responseSender;
    }

}
