'use strict';

var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');

var stompClient = null;
var username = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

var inputFile    = document.getElementById('input-file');
var fileName = null;
var isWait = null;

inputFile.addEventListener('change', function(){
  fileName = this.value;
  var enviarBtn = document.getElementById('enviarBtn');
  enviarBtn.click();
});    


function connect(event) {
    username = document.querySelector('#name').value.trim();

    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}


function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Não foi possivel conectar no servidor por websocket, tente novamente!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) { 
    var messageContent = messageInput.value.trim();

    if(fileName != null){
        var ini = fileName.lastIndexOf("\\");
        var end = fileName.length;
        fileName = fileName.substring(ini+1,end);
        messageContent = "Enviando Arquivo "+fileName;
    }
    if(isWait != null){
        messageContent="isWait=true;";
    }

    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
            content: messageContent,
            type: 'CHAT'
        };

        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
    fileName=null
}

async function responseCustomerService(message){
    await  sleep(2000);
    var messageResponse = {
        content:'',
        sender:''
    };
    if(isWait!=null){
        isWait = null;
    }
    var messageElement = document.createElement('li');

    messageResponse.sender = message.responseSender;
    messageResponse.content = message.responseContent;

    if(message.type === 'JOIN') {
        messageResponse.sender = "Atendimento Virtual";
        messageResponse.content = "Olá "+message.sender +" digite o que deseja.";
    }

    if(message.type === 'VALIDADO') {
        messageResponse.sender = "Atendimento Virtual";
        messageResponse.content = "Deseja algo mais, ou posso encerrar o atendimento ?";
    }
    
    messageElement.classList.add('chat-message');

    var avatarElement = document.createElement('i');
    var avatarText = document.createTextNode(messageResponse.sender[0]);
    avatarElement.appendChild(avatarText);
    avatarElement.style['background-color'] = getAvatarColor(messageResponse.sender);

    messageElement.appendChild(avatarElement);

    var usernameElement = document.createElement('span');
    var usernameText = document.createTextNode(messageResponse.sender);
    usernameElement.appendChild(usernameText);
    messageElement.appendChild(usernameElement);

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(messageResponse.content);

    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);
    messageArea.appendChild(messageElement);

    if(message.type === 'ARQUIVO') {
        await  sleep(2000);
        inputFile.click();
    }
    if(message.type === 'AGUARDE') {
        isWait="true";
        var enviarBtn = document.getElementById('enviarBtn');
        enviarBtn.click();
       
    }

    messageArea.scrollTop = messageArea.scrollHeight;
    if(message.type === 'ENCERRAR') {
        await  sleep(2000);
        document.location.reload();
    }  
    
   
    
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' Conectado';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' Saiu ';
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;

   
    responseCustomerService(message);
    
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 31 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}

usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)
