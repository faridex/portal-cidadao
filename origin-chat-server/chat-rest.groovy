from('rest:put:/message:')
    .setHeader("Access-Control-Allow-Origin", constant("*"))
    .to('knative:channel/chat-channel')
    .log('messagem recebida: ${body}');

from('rest:options:/message:')
    .setHeader("Access-Control-Allow-Origin", constant("*"))
    .setHeader("Access-Control-Allow-Methods", constant("PUT"))
    .setHeader("Access-Control-allow-Headers", constant("*"));