//camel-k: language=java
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import com.google.api.services.sheets.v4.model.ValueRange;

public class GoogleSheet extends RouteBuilder{
    @Override
    public void configure() throws Exception {
        String range = "A1";

        String clientId = "44514359558-8eformibhcmt0nnef15p2oi1kbppie1q.apps.googleusercontent.com";//System.getenv("CLIENT_ID");
        String clientSecret = "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";//System.getenv("CLIENT_SECRET");
        //String refreshToken = System.getenv("REFRESH_TOKEN");
        String spreadSheetID = "1RFtitJrCF54EuiufkeXPUIuK6JTbArx57N2cFieLiLg";//System.getenv("SPREADSHEET_ID");

        Supplier<Object> valueSupplier = new Supplier<Object>() {
            private String message;
            public void setMessage(String message){
                this.message = message;
            }
            
            @Override
            public Object get(){
                if( message == null){
                    return null;
                }

                ValueRange values = new ValueRange();
                List<List<Object>> rows = new ArrayList();
                List<Object> row = new ArrayList<Object>();
                //row.add(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG).format)
                row.add(System.currentTimeMillis());
                String[] messageParts = this.message.split(":");
                row.add(messageParts[0].trim());
                row.add(messageParts[1  ].trim());
                rows.add(row);
                values.setValues(rows);
                return values;
            }

        };

        getContext().getRegistry().bind("valueSupplier",valueSupplier);

        from("knative:channel/chat-channel")
            .routeId("ChatLogGoogleSheets")
            .log("Processando mensagem: ${body}")
            .bean("valueSupplier","setMessage")
            .setHeader("CamelGoogleSheets.values",valueSupplier)
            //.setHeader("CamelGoogleSheets.valueInputOption", constant("USER_ENTERED"))
            .to("google-sheets://data/append?spreadsheetId="+ spreadSheetID
                + "&range=" + range
                + "&clientId=" + clientId 
                + "&clientSecret=" + clientSecret
                //+ "&refreshToken=" + refreshToken
            );
    }


}
