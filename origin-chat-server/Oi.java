// camel-k: language=java

import org.apache.camel.builder.RouteBuilder;

public class Oi extends RouteBuilder {
  @Override
  public void configure() throws Exception {

      rest("/").post().to("direct:diga-oi");

      from("direct:diga-oi")
        .routeId("java")
        .setBody()
          .simple("Hello Camel K from ${routeId}")
        .to("log:info");

  }
}
