package com.cidadao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cidadao.entity.Pagamento;
import com.cidadao.service.ProcessarPagamentoService;

@RestController
@RequestMapping(value = "/processar-pagamento")
public class ProcessarPagamentoResource {
    
    @Autowired
    ProcessarPagamentoService processarPagamentoService;
   
    @GetMapping (value = "/")
    @CrossOrigin
    public String doGetMessage (){
        
       return "Serviço Ativo..";
    }

    
}
