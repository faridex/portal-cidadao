package com.cidadao.service;

import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.cidadao.entity.Pagamento;
import com.google.gson.Gson;

//@Slf4j
//@RequiredArgsConstructor
@Service
public class ProcessarPagamentoService { 
    
    Logger logger = Logger.getLogger(ProcessarPagamentoService.class.getName());
    @Value("${topic.iptu.retorno.pagamento.producer}")
    private String iptuRetornoPagamentoProducer; 

    @Value("${topic.iptu.requisicao.pagamento.consumer}")
    private String iptuRequisicaoPagamentoConsumer; 

    private KafkaTemplate<String, String> kafkaTemplate;


    @Autowired
    ProcessarPagamentoService(KafkaTemplate<String, String> kafkaTemplate) {
      this.kafkaTemplate = kafkaTemplate;
      
    }


    @KafkaListener(topics = "${topic.iptu.requisicao.pagamento.consumer}")
    public void consumeRequisicaoCartao(ConsumerRecord<String, String> payload){
        Gson gson = new Gson();
        String valueReceived = payload.value();
        logger.info("Lendo Mensagem de "+ iptuRequisicaoPagamentoConsumer);
        logger. info("Valor Recebido ["+ valueReceived+"]");
        Pagamento pagamento = gson.fromJson(valueReceived, Pagamento.class);
        if(pagamento.formaPagamento.equals("Cartão de Crédito"))
            pagamento.situacaoPagamento="Pagamento Cartão de Crédito Selecionado.";
        else{
            pagamento.situacaoPagamento="Pagamento Pix Selecionado.";
        }    
        String jsonPaymentRetorno = gson.toJson(pagamento);
        sendReturnPayment(jsonPaymentRetorno);
        wait(pagamento);
        
        pagamento.situacaoPagamento="Pagamento Enviado para Processamento.";
        jsonPaymentRetorno = gson.toJson(pagamento);
        sendReturnPayment(jsonPaymentRetorno);
        wait(pagamento);

        pagamento.situacaoPagamento="Pagamento Processando,";
         jsonPaymentRetorno = gson.toJson(pagamento);
        sendReturnPayment(jsonPaymentRetorno);
        wait(pagamento);
        
        pagamento.situacaoPagamento="Pagamento Validado.";
        jsonPaymentRetorno = gson.toJson(pagamento);
        sendReturnPayment(jsonPaymentRetorno);
    }

   
    private void wait(Pagamento pagamento) {
        Long nValue = Long.parseLong(pagamento.cpf);
        long qtdWait = 0;
        if(nValue.longValue() >= 50){
            qtdWait=7000;
        }else{
            qtdWait=4000;
        }
        try{
            Thread.sleep(qtdWait);
        }catch(Exception e){

        }
    } 

    
    

    public void sendReturnPayment(String jsonPaymentoRetorno){
        
        logger.info("Enviando mensagem para iptu-retorno-pagamento-cartao" + jsonPaymentoRetorno);

        kafkaTemplate.send(iptuRetornoPagamentoProducer, jsonPaymentoRetorno);
    }  
    
      
    



}
