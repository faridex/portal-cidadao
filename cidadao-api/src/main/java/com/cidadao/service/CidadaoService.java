package com.cidadao.service;

import java.util.List;

import com.cidadao.entity.Cidadao;




public class CidadaoService {
    
    
    public void doIncluirCidadao(Cidadao cidadao){
        cidadao.persist();
    }

    public List<Cidadao> doGetAll(){
        return Cidadao.listAll();
    }
}
