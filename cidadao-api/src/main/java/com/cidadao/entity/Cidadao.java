package com.cidadao.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;

@Entity
public class Cidadao extends PanacheEntity{
  
    public String nome;
    public String cpf;
    public Long idade;

}
