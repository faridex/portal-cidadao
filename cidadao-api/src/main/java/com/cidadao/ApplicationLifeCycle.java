package com.cidadao;

import java.util.logging.Logger;

import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;

@ApplicationScoped
public class ApplicationLifeCycle {

  
  Logger logger = Logger.getLogger(this.getClass().getName());

  void onStart(@Observes StartupEvent ev) {
    logger.info(">>> The application is starting with PROFILE ### " + ProfileManager.getActiveProfile() + " ###");
  }
}