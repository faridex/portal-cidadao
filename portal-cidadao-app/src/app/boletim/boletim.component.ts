import { Component, Input } from '@angular/core';
import { BoletimService } from './boletim.service';
import { AlunoBoletim } from '../model/alunoBoletim';


import { WebSocketService } from '../web-socket/websocket.service';
import { interval } from "rxjs/internal/observable/interval";
import { switchMap, takeWhile } from 'rxjs';



@Component({
  selector: 'app-boletim',
  templateUrl: './boletim.component.html',
  styleUrls: ['./boletim.component.css']
})
export class BoletimComponent {
  
  public alunoBoletim!: AlunoBoletim;
  
  nuMatricula = null;
  public messageError = '';
  
  public isHideModalPagamento:string = 'false';
  public tamanhoRetorno = 0;
  public emProcessamento = 'false';
  public emProcessamentoConsulta = 'false';
   

  constructor(private boletimService: BoletimService
        ,public webSocketService : WebSocketService) { 

          this.webSocketService.connect("ws://pagamento-api-portal-cidadao.apps.cluster-d6chv.d6chv.sandbox2781.opentlc.com/iptu-pagamento/123")
          //this.retornoPagamento = this.webSocketService.receivedData;
          
          
        }

  fecharModalPagamento(){
    this.isHideModalPagamento='false';
  }
 
  
  
 
 
 
  consultarRegistros() {
    this.emProcessamentoConsulta = 'true';
    console.log('matricula :: ' + this.nuMatricula);
    
    this.boletimService.get(this.nuMatricula)
      .subscribe({
        next: (data) => {
          this.alunoBoletim = data;
          console.log(data);
          this.emProcessamentoConsulta = 'false';
        },
        error: (e) => {
          console.log(e);
          this.emProcessamentoConsulta = 'false';
        }
      });
  }
}