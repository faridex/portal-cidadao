import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlunoBoletim } from '../model/alunoBoletim';


//const baseurl = 'http://endereco-api-webserver-acm.apps.bvqq9-dev.sandbox3042.opentlc.com';

@Injectable({
  providedIn: 'root'
})
export class BoletimService {
  
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  

  get(id: any): Observable<AlunoBoletim> {
    return this.http.get<AlunoBoletim>(`/portal-cidadao-api/aluno/boletim/filtros?nu_matricula=${id}&user_key=portal-cidadao-key`);
  }

  
}
