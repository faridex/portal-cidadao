import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Iptu } from '../model/iptu';
import { Pagamento } from '../model/pagamento';

//const baseurl = 'http://endereco-api-webserver-acm.apps.bvqq9-dev.sandbox3042.opentlc.com';

@Injectable({
  providedIn: 'root'
})
export class IptuService {
  
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  

  get(id: any): Observable<Iptu[]> {
    return this.http.get<Iptu[]>(`/portal-cidadao-api/iptu/filtros?cpf=${id}&user_key=portal-cidadao-key`);
  }

  getPaymentoByFilter(id: any): Observable<Pagamento[]> {
    return this.http.get<Pagamento[]>(`/portal-cidadao-api/iptu/payment-filtros?cpf=${id}&user_key=portal-cidadao-key`);
  }

  realizarPagamentoCartaoCredito(pagamento: Pagamento){
    return this.http.post<Pagamento>('/portal-cidadao-api/iptu/payment/credit-card?user_key=portal-cidadao-key', pagamento);
    
  }

  realizarPagamentoPix(pagamento: Pagamento){
    return this.http.post<Pagamento>('/portal-cidadao-api/iptu/payment/pix?user_key=portal-cidadao-key', pagamento);
    
  }
}
