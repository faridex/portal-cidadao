import { Endereco } from "./endereco"
export class DadosPessoais {
    cpf!: string;
    nome!: string;
    endereco!: Endereco;
}
