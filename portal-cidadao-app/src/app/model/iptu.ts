import { DadosPessoais } from "./dados-pessoais"
export class Iptu {
    numeroInscricao!: string;
    valorIptu!: string;
    situacaoPagamento!: string;
    cpf!: string;
    public dadosPessoais!: DadosPessoais;
}
