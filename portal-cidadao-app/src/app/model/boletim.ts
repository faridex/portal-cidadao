
export class Boletim {
    

    vrNotaPortuguesB1!: string;
    vrNotaPortuguesB2!: string;
    vrNotaPortuguesB3!: string;
    vrNotaPortuguesB4!: string;
    vrMediaPortugues=0;

    vrNotaMatematicaB1!: string;
    vrNotaMatematicaB2!: string;
    vrNotaMatematicaB3!: string;
    vrNotaMatematicaB4!: string;
    vrMediaMatematica!: string;

    vrNotaHistoriaB1!: string;
    vrNotaHistoriaB2!: string;
    vrNotaHistoriaB3!: string;
    vrNotaHistoriaB4!: string;
    vrMediaHistoria!: string;

    vrNotaGeografiaB1!: string;
    vrNotaGeografiaB2!: string;
    vrNotaGeografiaB3!: string;
    vrNotaGeografiaB4!: string;
    vrMediaGeografia!: string;
}