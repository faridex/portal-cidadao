export class Endereco {
    bairro!: string;
    cep!: string;
    cidade!: string;
    complemento!: string;
    logradouro!: string;
    uf!: string;
    versao!: string;
}
