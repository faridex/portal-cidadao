export class Pagamento {
    formaPagamento!: string;
    nsuPagamento!: string;
    cpf!: string;
    valorIptu!: string;
    numeroInscricao!: string;
    situacaoPagamento!: string;
}