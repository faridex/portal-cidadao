// kamel run --secret processar-pagamento-cartao-props ProcessarPagamentoCartaoConsumer.java --dev
// camel-k: language=java dependency=mvn:org.apache.camel.quarkus:camel-quarkus-kafka dependency=mvn:io.strimzi:kafka-oauth-client:0.7.1.redhat-00003

import org.apache.camel.builder.RouteBuilder;

public class ProcessarPagamentoCartaoConsumer extends RouteBuilder{
    @Override
    public void configure() throws Exception {
        log.info("Iniciando Execução Consumer Pagamento Cartão de Credito: Kafka -> Log ");

        from("kafka:{{consumer.topic}}")
            .routeId("FromKafka2Log")
            .log("${body}");
    }
}
