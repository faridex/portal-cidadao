package com.cidadao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cidadao.entity.Boletim;
import com.cidadao.entity.NotaAluno;
import com.cidadao.service.BoletimService;

@Path("/boletim") 
public class BoletimResource {

    @Inject
    BoletimService boletimService; 

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NotaAluno> doGetByFilter(
        @QueryParam("nu_matricula") Long nuMatricula
    ) {
        List<NotaAluno> notas = boletimService.doGetByFilter(nuMatricula);
        return notas;
    }

    @GET
    @Path("/boletim/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Boletim doGetBoletimByFilter(
        @QueryParam("nu_matricula") Long nuMatricula
    ) {
        List<NotaAluno> notas = boletimService.doGetByFilter(nuMatricula);
        Boletim boletim = new Boletim();
        for(NotaAluno notaAluna: notas){
             if(notaAluna.nuDisciplina.intValue()==1){
                mountNotasPortugues(boletim, notaAluna);
             }else if(notaAluna.nuDisciplina.intValue()==2){
                 mountNotasMatematica(boletim, notaAluna);
             }else if(notaAluna.nuDisciplina.intValue()==3){
                 mountNotasGeografia(boletim, notaAluna);
             }else if(notaAluna.nuDisciplina.intValue()==4){
                 mountNotasHistoria(boletim, notaAluna);
             }
        }
        mountMeidaDisciplina(boletim);
        return boletim;
    }

    private void mountMeidaDisciplina(Boletim boletim) {
        boletim.vrMediaGeografia=(boletim.vrNotaGeografiaB1+boletim.vrNotaGeografiaB2+boletim.vrNotaGeografiaB3+boletim.vrNotaGeografiaB4)/4;
        boletim.vrMediaHistoria=(boletim.vrNotaHistoriaB1+boletim.vrNotaHistoriaB2+boletim.vrNotaHistoriaB3+boletim.vrNotaHistoriaB4)/4;
        boletim.vrMediaPortugues=(boletim.vrNotaPortuguesB1+boletim.vrNotaPortuguesB2+boletim.vrNotaPortuguesB3+boletim.vrNotaPortuguesB4)/4;
        boletim.vrMediaMatematica=(boletim.vrNotaMatematicaB1+boletim.vrNotaMatematicaB2+boletim.vrNotaMatematicaB3+boletim.vrNotaMatematicaB4)/4;
    }

    private void mountNotasPortugues(Boletim boletim, NotaAluno notaAluna) {
        if(notaAluna.nuBimestre.intValue()==1 && notaAluna.nuDisciplina.intValue()==1){
            boletim.vrNotaPortuguesB1=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==2 && notaAluna.nuDisciplina.intValue()==1){
            boletim.vrNotaPortuguesB2=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==3 && notaAluna.nuDisciplina.intValue()==1){
            boletim.vrNotaPortuguesB3=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==4 && notaAluna.nuDisciplina.intValue()==1){
            boletim.vrNotaPortuguesB4=notaAluna.vrNota.doubleValue();
        }
    }

     private void mountNotasMatematica(Boletim boletim, NotaAluno notaAluna) {
        if(notaAluna.nuBimestre.intValue()==1 && notaAluna.nuDisciplina.intValue()==2){
            boletim.vrNotaMatematicaB1=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==2 && notaAluna.nuDisciplina.intValue()==2){
            boletim.vrNotaMatematicaB1=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==3 && notaAluna.nuDisciplina.intValue()==2){
            boletim.vrNotaMatematicaB3=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==4 && notaAluna.nuDisciplina.intValue()==2){
            boletim.vrNotaMatematicaB4=notaAluna.vrNota.doubleValue();
        }
    }

    private void mountNotasGeografia(Boletim boletim, NotaAluno notaAluna) {
        if(notaAluna.nuBimestre.intValue()==1 && notaAluna.nuDisciplina.intValue()==3){
            boletim.vrNotaGeografiaB1=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==2 && notaAluna.nuDisciplina.intValue()==3){
            boletim.vrNotaGeografiaB2=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==3 && notaAluna.nuDisciplina.intValue()==3){
            boletim.vrNotaGeografiaB3=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==4 && notaAluna.nuDisciplina.intValue()==3){
            boletim.vrNotaGeografiaB4=notaAluna.vrNota.doubleValue();
        }
    }

     private void mountNotasHistoria(Boletim boletim, NotaAluno notaAluna) {
        if(notaAluna.nuBimestre.intValue()==1 && notaAluna.nuDisciplina.intValue()==4){
            boletim.vrNotaHistoriaB1=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==2 && notaAluna.nuDisciplina.intValue()==4){
            boletim.vrNotaHistoriaB2=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==3 && notaAluna.nuDisciplina.intValue()==4){
            boletim.vrNotaHistoriaB3=notaAluna.vrNota.doubleValue();
        }
        if(notaAluna.nuBimestre.intValue()==4 && notaAluna.nuDisciplina.intValue()==4){
            boletim.vrNotaHistoriaB4=notaAluna.vrNota.doubleValue();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/load-data")
    public  List<NotaAluno> doLoadAll(
        @QueryParam("inicio_carca") Long inicio_carga

    ) {
        long matricula=inicio_carga;
        long limiteBlocoMatricul = matricula+5;
        Random random = new Random();
        for(int t=5;t<10;t++){
            for(int i=inicio_carga.intValue();i < limiteBlocoMatricul;i++){
                for(int nuBimestre=1;nuBimestre<5;nuBimestre++){
                    for(int nuDisciplina=1;nuDisciplina<5;nuDisciplina++){
                        int randomNumber = random.nextInt(10) + 1;
                        NotaAluno notaAluno = new NotaAluno();
                        notaAluno.dtNota=LocalDate.now();
                        notaAluno.nuDisciplina= new BigInteger(String.valueOf(nuDisciplina));
                        notaAluno.nuNota=new BigInteger(String.valueOf(NotaAluno.count()+1));   
                        notaAluno.nuAno=new BigInteger("2023"); 
                        notaAluno.nuBimestre= new BigInteger(String.valueOf(nuBimestre));
                        notaAluno.nuMatricula= new BigInteger(String.valueOf(matricula));
                        notaAluno.vrNota=new BigDecimal(randomNumber);    
                        boletimService.doLoadAll(notaAluno);
                    }
                }
                
            }
            matricula++;
        }
        List<NotaAluno> notas = boletimService.doGetAll();
        return notas;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public  List<NotaAluno> doGetAll() {
        List<NotaAluno> notas = boletimService.doGetAll();
        return notas;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void doIncluirNota(NotaAluno notaAluno){
        boletimService.doIncluirNota(notaAluno);
    }


}