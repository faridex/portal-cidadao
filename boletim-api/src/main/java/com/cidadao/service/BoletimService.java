package com.cidadao.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.cidadao.entity.BoletimAluno;
import com.cidadao.entity.NotaAluno;




@ApplicationScoped
public class BoletimService {
    Logger logger = Logger.getLogger(BoletimService.class.getName());
    
    @ConfigProperty(name = "aluno.abortar.consulta") 
    Boolean abortarConsulta;

    @ConfigProperty(name = "aluno.delay.consulta") 
    Long delayConsulta;

    @ConfigProperty(name = "aluno.versao") 
    Long versao;

    
    
   
    public List<NotaAluno> doGetByFilter(Long nuMatricula){
        logger.info("Consultando Aluno...");
        if(abortarConsulta != null && abortarConsulta){
            logger.info("Abortando....");
            throw new RuntimeException();
        }else if(delayConsulta != null && delayConsulta.longValue() >0){
            try {
                logger.info("Aplicando Delay de "+ delayConsulta.longValue());
                Thread.sleep(delayConsulta);
            } catch (Exception e) {
                throw new RuntimeException();
            } 
        }
        String command = 
        "select nu_nota, "+
        " nu_disciplina, "+
        " nu_ano, "+
        " nu_bimestre, "+
        " nu_matricula, "+
        " vr_nota, "+
        " max(dt_nota) as dt_nota  "+
        " from public.NotaAluno "+
        " where nu_matricula =  "+ nuMatricula +
        " group by nu_nota, "+
        " nu_disciplina, "+
        " nu_ano, "+
        " nu_bimestre, "+
        " nu_matricula, "+
        " vr_nota  ";
        System.out.println(command);
        EntityManager em = NotaAluno.getEntityManager();
        Query query = em.createNativeQuery(command,NotaAluno.class);
        List<NotaAluno> notas = query.getResultList();
        return notas;
    }

    public List<NotaAluno> doGetAll() {
        return NotaAluno.listAll();
    }
    

    @Transactional
    public void doLoadAll(NotaAluno notaAluno) {
        notaAluno.persist();
        logger.info("Carregando Matricula"+notaAluno.nuMatricula);
    }
    
    @Transactional
    public void doIncluirNota(NotaAluno notaAluno){
        EntityManager em = BoletimAluno.getEntityManager();
        try{
            Object maxId = BoletimAluno.find("select NU_BOLETIM from BoletimAluno where NU_ANO = (select MAX(NU_ANO) from BoletimAluno where NU_ALUNO = "+ notaAluno.nuMatricula +")",Long.class)
                                  .singleResult();
        }catch(Exception e){
            Random random = new Random();
            for(int nuBimestre=1;nuBimestre<5;nuBimestre++){
                int randomNumber = random.nextInt(10) + 1;
                notaAluno.dtNota=LocalDate.now();
                //notaAluno.nuDisciplina= new BigInteger(String.valueOf(nuDisciplina));
                notaAluno.nuNota=new BigInteger(String.valueOf(NotaAluno.count()+1));   
                notaAluno.nuAno=new BigInteger("2023"); 
                notaAluno.nuBimestre= new BigInteger(String.valueOf(nuBimestre));
                //notaAluno.nuMatricula= new BigInteger(String.valueOf(matricula));
                notaAluno.vrNota=new BigDecimal(randomNumber);    
                doLoadAll(notaAluno);
            }
        }
       // System.out.println("val"+maxId);
    }
    
   
}
