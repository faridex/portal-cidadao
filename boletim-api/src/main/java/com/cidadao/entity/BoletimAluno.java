package com.cidadao.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
@Table(name="BoletimAluno")
public class BoletimAluno extends PanacheEntityBase{
    @Id
    @Column(name =  "nu_boletim")
    public BigInteger nuBoletim;
    @Column(name =  "nu_aluno")
    public BigInteger nuAluno;
    @Column(name =  "nu_ano")
    public BigInteger nuAno;
    @Column(name =  "vr_media_portugues")
    public BigDecimal vrMediaPortugues;
    @Column(name =  "vr_media_matematica")
	public BigDecimal vrMediaMatematica;
    @Column(name =  "vr_media_historia")
	public BigDecimal vrMediaHistoria;
    @Column(name =  "vr_media_geografia")
	public BigDecimal vrMediaGeografia;
}
