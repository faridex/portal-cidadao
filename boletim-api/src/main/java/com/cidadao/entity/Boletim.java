package com.cidadao.entity;

public class Boletim {
    public double vrNotaPortuguesB1=0;
    public double vrNotaPortuguesB2=0;
    public double vrNotaPortuguesB3=0;
    public double vrNotaPortuguesB4=0;
    public double vrMediaPortugues=0;

    public double vrNotaMatematicaB1=0;
    public double vrNotaMatematicaB2=0;
    public double vrNotaMatematicaB3=0;
    public double vrNotaMatematicaB4=0;
    public double vrMediaMatematica=0;

    public double vrNotaHistoriaB1=0;
    public double vrNotaHistoriaB2=0;
    public double vrNotaHistoriaB3=0;
    public double vrNotaHistoriaB4=0;
    public double vrMediaHistoria=0;

    public double vrNotaGeografiaB1=0;
    public double vrNotaGeografiaB2=0;
    public double vrNotaGeografiaB3=0;
    public double vrNotaGeografiaB4=0;
    public double vrMediaGeografia=0;


}