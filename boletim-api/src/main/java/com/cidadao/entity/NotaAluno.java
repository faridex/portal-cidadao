package com.cidadao.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class NotaAluno extends PanacheEntityBase{
    @Id
    @Column(name =  "nu_nota")
    public BigInteger nuNota;
    @Column(name =  "nu_disciplina")
    public BigInteger nuDisciplina;
    @Column(name =  "nu_ano")
    public BigInteger nuAno;
    @Column(name =  "nu_bimestre")
    public BigInteger nuBimestre;
    @Column(name =  "nu_matricula")
    public BigInteger nuMatricula;
    @Column(name =  "dt_nota")
    public LocalDate dtNota;
    @Column(name =  "vr_nota")
    public BigDecimal vrNota;





    
}
