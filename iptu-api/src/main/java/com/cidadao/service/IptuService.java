package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.api.DadosPessoaisApi;
import com.cidadao.api.PagamentoApi;
import com.cidadao.entity.DadosPessoais;
import com.cidadao.entity.Iptu;
import com.cidadao.entity.Pagamento;

@ApplicationScoped
public class IptuService {
    Logger logger = Logger.getLogger(IptuService.class.getName());

    @RestClient
    DadosPessoaisApi dadosPessoaisApi;

    @RestClient
    PagamentoApi pagamentoApi;

    static Map<String, Iptu> iptus = new HashMap<String, Iptu>();

    public List<Iptu> doGetByFilter(String cpf) {
        Iptu iptu = new Iptu();
        DadosPessoais dadosPessoais = dadosPessoaisApi.doGetByFilter(cpf);
        iptu.dadosPessoias = dadosPessoais;
        Iptu _iptu = iptus.get(cpf);
        List<Iptu> _iptus = new ArrayList<Iptu>();
        iptu.numeroInscricao=_iptu.numeroInscricao;
        iptu.valorIptu=_iptu.valorIptu;
        iptu.situacaoPagamento=_iptu.situacaoPagamento;
        iptu.cpf= dadosPessoais.cpf;
        _iptus.add(iptu);
        return _iptus;
    }

    @PostConstruct
    private void mountDados() {
        NumberFormat dfIncricao = new DecimalFormat("00000000");
        for (int i = 0; i <= 100; i++) {
            Iptu iptu = new Iptu();
            DadosPessoais dadosPessoais = getDadosPessoais(i);
            iptu.dadosPessoias = dadosPessoais;
            iptu.numeroInscricao = "INC"+dfIncricao.format(i);
            iptu.situacaoPagamento = "Aguardando Pagamento";
            iptu.cpf= dadosPessoais.cpf;
            double vr = (i*3)+78;
            iptu.valorIptu= vr;
            iptus.put(dadosPessoais.cpf, iptu);
        }
    }

    private DadosPessoais getDadosPessoais(int sequencia) {
        NumberFormat df = new DecimalFormat("00000000000");
        String cpf = df.format(sequencia);
        logger.info("Indo buscar sequencia " + sequencia);
        DadosPessoais dadosPessoais = dadosPessoaisApi.doGetByFilter(cpf);
        return dadosPessoais;
    }

    public List<Iptu> doGetAll() {
        List<Iptu> _iptus = new ArrayList<Iptu>();
        _iptus.addAll(iptus.values());
        return _iptus;
    }

    public Pagamento doPayment(Pagamento pagamento){
        Pagamento _pagamento =  pagamentoApi.doSendPayment(pagamento);
        Iptu _iptu = iptus.get(pagamento.cpf);
        _iptu.situacaoPagamento=_pagamento.situacaoPagamento;
        iptus.put(pagamento.cpf, _iptu);
        return _pagamento;
    }

    public List<Pagamento> doGetPaymentForFilter(String cpf){
        List<Pagamento> _pagamentos =  pagamentoApi.doGetPaymentForFilter(cpf);
       return _pagamentos;
    }
}
