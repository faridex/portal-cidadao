package com.cidadao;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.cidadao.entity.Iptu;
import com.cidadao.entity.Pagamento;
import com.cidadao.service.IptuService;



@Path("/iptu")
public class IptuResource {
    Logger logger = Logger.getLogger(IptuResource.class.getName());
    @Inject
    IptuService iptuService;

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGetByFilter(
        @QueryParam("cpf") String cpf
    ) 
    {
        logger.info("Recebendo Requiscao com cpf ["+cpf+"]:q!");
        List<Iptu> iptus = iptuService.doGetByFilter(cpf);
        return Response.ok(iptus).build();
    }
        
        
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGetAll() {
        logger.info("Recebendo Requiscao buscar todos");
        List<Iptu> agendamentos = iptuService.doGetAll();
        return Response.ok(agendamentos).build();
    }

    @POST
    @Path("/payment/credit-card")
    public Response doPaymentCreditCard(Pagamento pagamento){
        pagamento.formaPagamento="Cartão de Crédito";
        Pagamento _pagamento = iptuService.doPayment(pagamento);
        return Response.ok(_pagamento).build();
    }

    @POST
    @Path("/payment/pix")
    public Response doPaymentPix(Pagamento pagamento){
        pagamento.formaPagamento="Pix";
        Pagamento _pagamento = iptuService.doPayment(pagamento);
        return Response.ok(_pagamento).build();
    }

    @GET
    @Path("/payment-filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Response doGetPayementoByFilter(@QueryParam("cpf") String cpf){
        List<Pagamento> payments = iptuService.doGetPaymentForFilter(cpf);
        return Response.ok(payments).build(); 
    }


    @Path("/confirmar-pix")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String doMountPageConfirmarPix(@QueryParam("inc") String inc
    ,@QueryParam("vic") String vic){
        StringBuffer sb = new StringBuffer();
        sb
        .append("<!DOCTYPE html>")
        .append("<html>")
        .append("<head>")
        .append("    <meta charset=\"UTF-8\">")
        .append("    <title>Pix</title>")
        .append("    <style>")
        .append("        header {")
        .append("            background-color: #3498db;")
        .append("            color: white;")
        .append("            padding: 20px;")
        .append("            text-align: center;")
        .append("        }")
        .append("       body {")
        .append("            font-family: Arial, sans-serif;")
        .append("            margin: 0;")
        .append("            padding: 0;")
        .append("            background-color: #f2f2f2;")
        .append("        }")
        .append("        .conteudo {")
        .append("            max-width: 800px;")
        .append("            margin: 20px auto;")
        .append("            background-color: white;")
        .append("            padding: 20px;")
        .append("            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);")
        .append("        }")
        .append("        footer {")
        .append("            background-color: #333;")
        .append("            color: white;")
        .append("            text-align: center;")
        .append("            padding: 10px;")
        .append("        }")
        .append("    </style>")
        .append("</head>")
        .append("<body>")
        .append("    <header>")
        .append("        <h1>Confirmar Pix</h1>")
        .append("    </header>")
        .append("    <div class=\"conteudo\">")
        .append("        <form action=\"processar-pix\" method=\"post\">")
        .append("        <p>")
        .append("            Informe o codigo de Confirmação <input type=\"text\" value='' name='codigoaut'>")
        .append("            <input type=\"submit\" value=\"Confirmar\"/>")
        .append("            <input type='hidden' value='").append(inc).append("' name='inc'/>")
        .append(             "<input type='hidden' value='").append(vic).append("' name='vic'/>")
        .append("        </p>")
        .append("    </form>")
        .append("    </div>")
        .append("    <footer>")
        .append("    </footer>")
        .append("</body>")
        .append("</html>");
        return sb.toString();
    }

    @Path("/processar-pix")
    @POST
    @Produces(MediaType.TEXT_HTML)
    public String doProcessarPix(
        @FormParam("inc") String inc
       ,@FormParam("vic") String vic
       ,@FormParam("codigoaut") String codigoaut
       ){
        boolean icConfirmado = false;
        String msg = "Codigo de Confirmação não confere !!";
        if(codigoaut != null && codigoaut.equals("main")){
            icConfirmado=true;
            msg = "PIX Realizado com Sucesso !!";
        }
        StringBuffer sb = new StringBuffer();
        sb
        .append("<!DOCTYPE html>")
        .append("<html>")
        .append("<head>")
        .append("    <meta charset=\"UTF-8\">")
        .append("    <title>Pix</title>")
        .append("    <style>")
        .append("        header {")
        .append("            background-color: #3498db;")
        .append("            color: white;")
        .append("            padding: 20px;")
        .append("            text-align: center;")
        .append("        }")
        .append("       body {")
        .append("            font-family: Arial, sans-serif;")
        .append("            margin: 0;")
        .append("            padding: 0;")
        .append("            background-color: #f2f2f2;")
        .append("        }")
        .append("        .conteudo {")
        .append("            max-width: 800px;")
        .append("            margin: 20px auto;")
        .append("            background-color: white;")
        .append("            padding: 20px;")
        .append("            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);")
        .append("        }")
        .append("        footer {")
        .append("            background-color: #333;")
        .append("            color: white;")
        .append("            text-align: center;")
        .append("            padding: 10px;")
        .append("        }")
        .append("    </style>")
        .append("</head>")
        .append("<body>")
        .append("    <header>")
        .append("        <h1>Confirmar Pix</h1>")
        .append("    </header>")
        .append("    <div class=\"conteudo\">")
        .append("        <p>")
        .append("            <h2>").append(msg).append("</h2>")
        .append("        </p>")
        .append("    </div>")
        .append("    <footer>")
        .append("    </footer>")
        .append("</body>")
        .append("</html>");
        if(icConfirmado){
            Pagamento pagamento = new Pagamento();
            pagamento.cpf=inc;
            doPaymentPix(pagamento);
        }
        
        return sb.toString();
    }
   
}