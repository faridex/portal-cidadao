package com.cidadao.api;



import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.cidadao.entity.Pagamento;


@Path("/pagamento")
@RegisterRestClient
public interface PagamentoApi {

    @POST
    public Pagamento doSendPayment(Pagamento pagamento);

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pagamento> doGetPaymentForFilter(@QueryParam("cpf") String cpf);
    

}