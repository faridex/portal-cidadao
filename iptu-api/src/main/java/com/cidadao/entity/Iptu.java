package com.cidadao.entity;
public class Iptu {
    public Long versao;
    public DadosPessoais dadosPessoias;
    public Double valorIptu;
    public String numeroInscricao;
    public String situacaoPagamento;
    public String cpf;
}
