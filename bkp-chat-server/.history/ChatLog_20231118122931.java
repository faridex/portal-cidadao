// camel-k: language=java

import org.apache.camel.builder.RouteBuilder;

public class ChatLog extends RouteBuilder {
  @Override
  public void configure() throws Exception {

      // Write your routes here, for example:
      from("knative:channel/chat-channel")
        .routeId("route-chat-log")
        .lot("Evento Recebido ${body}")
        .to("log:info");

  }
}
