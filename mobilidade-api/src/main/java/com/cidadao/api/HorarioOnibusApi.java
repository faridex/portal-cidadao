package com.cidadao.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/api/horarios")
@RegisterRestClient(baseUri="http://gistapis.etufor.ce.gov.br:8081/")
@ApplicationScoped
public interface HorarioOnibusApi {

    @GET
    @Path("/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public String doGetByFilter(
            @PathParam("code") String code,
            @QueryParam("data") String data
    );

    class HorarioLinha{
        public String postoControle;
        public List<Horario> horarios;
      
    }

    class Horario{
       public String horario;
       public String acessivel;
       public String tabela;
    }
}
