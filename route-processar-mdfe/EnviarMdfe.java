// camel-k: language=java

import org.apache.camel.builder.RouteBuilder;

public class EnviarMdfe extends RouteBuilder {
  @Override
  public void configure() throws Exception {
      rest("/").post().to("direct:enviarMdfe");


      // Write your routes here, for example:
      from("direct:enviarMdfe")
        .routeId("enviarMdfe-Java")
        .setBody()
          .simple("Hello Camel K from ${routeId}")
        .to("log:info");

        

  }
}
