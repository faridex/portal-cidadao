import org.apache.camel.builder.RouteBuilder;
public class RouteProcessarMdfe extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("kafka:{{consumer.topic}}")
        .routeId("FromKafka2Log")
        .log(":: Evento Recebido:: ${body}")
        .process(exchange ->{
            System.out.println("::: Fazendo Parser do payloado MDFe.");
            String json = exchange.getIn().getBody(String.class);
        });
    }
    
}
