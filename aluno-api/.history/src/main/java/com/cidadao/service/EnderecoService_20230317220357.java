package com.cidadao.service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import com.cidadao.entity.Endereco;



@ApplicationScoped
public class EnderecoService {
    
    static Map<String,Endereco> enderecos = new HashMap<String,Endereco>();
    public Endereco doRecuperarEnderecoCep(String cep){
       return enderecos.get(cep);
    }

    @PostConstruct
    private void mountAgendamento(){
        for(int i=0;i<100;i++){
            Endereco endereco = getEndereco(i);
            enderecos.put(endereco.cep, endereco);
        }
    }

    
    private Endereco getEndereco(int sequencia){
        NumberFormat df = new DecimalFormat("00000000");
        Endereco endereco = new Endereco();
        endereco.cep=df.format(sequencia);
        if(sequencia %2==0){
            endereco.logradouro="Avenida vai que vai "+sequencia;
        }else{
            endereco.logradouro="Rua bem Ali número "+sequencia;
        }
        endereco.bairro="Algum bairro do lado v"+sequencia;
        endereco.cidade="Cidade do Teste "+sequencia;
        endereco.uf="YU";
        endereco.complemento="Cond. du tatu";
        return endereco;
    }
    
    public List<Endereco> doRecuperarEnderecos() {
        List<Endereco> _enderecos = new ArrayList<Endereco>();
        _enderecos.addAll(enderecos.values());
        return _enderecos;
    }
}
