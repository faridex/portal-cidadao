package com.cidadao.service;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import com.cidadao.api.BoletimApi;
import com.cidadao.entity.Aluno;
import com.cidadao.entity.AlunoBoletim;
import com.cidadao.entity.Boletim;




@ApplicationScoped
public class AlunoService {
    Logger logger = Logger.getLogger(AlunoService.class.getName());
    
    @ConfigProperty(name = "aluno.abortar.consulta") 
    Boolean abortarConsulta;

    @ConfigProperty(name = "aluno.delay.consulta") 
    Long delayConsulta;

    @ConfigProperty(name = "aluno.versao") 
    Long versao;

    @RestClient
    BoletimApi boletimApi;
    
    
   
    public Aluno doGetByFilter(Long nuAluno){
        logger.info("Consultando Aluno...");
        if(abortarConsulta != null && abortarConsulta){
            logger.info("Abortando....");
            throw new RuntimeException();
        }else if(delayConsulta != null && delayConsulta.longValue() >0){
            try {
                logger.info("Aplicando Delay de "+ delayConsulta.longValue());
                Thread.sleep(delayConsulta);
            } catch (Exception e) {
                throw new RuntimeException();
            } 
        }
        Aluno aluno = Aluno.findById(nuAluno);
        
        
        return aluno;
    }

    public List<Aluno> doGetAll() {
        return Aluno.listAll();
    }
    

    @Transactional
    public List<Aluno> doLoadAll(Long inicio_carga) {
        long matricula=inicio_carga;
        long limiteBlocoMatricul = matricula+50;
        for(int t=5;t<10;t++){
            for(int i=inicio_carga.intValue();i < limiteBlocoMatricul;i++){
                Aluno aluno = new Aluno();
                aluno.noAluno="Aluno "+matricula;
                aluno.nuMatricula= matricula;
                aluno.noTurma="Turma "+t+"°";
                aluno.persist();
                matricula++;
                logger.info("Carregando "+aluno.noAluno);
            }
        }
        
        return Aluno.listAll();
    }

    public AlunoBoletim doGetBoletimByFilter(Long nuMatricula) {
        Boletim boletim = boletimApi.doGetBoletimByFilter(nuMatricula);
        Aluno aluno = doGetByFilter(nuMatricula);
        AlunoBoletim alunoBoletim = new AlunoBoletim();
        alunoBoletim.aluno = aluno;
        alunoBoletim.boletim = boletim;
        return alunoBoletim;
    }
    
   
}
