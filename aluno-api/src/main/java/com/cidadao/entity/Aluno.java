package com.cidadao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Aluno extends PanacheEntityBase{
    @Id
    @Column(name =  "nu_matricula")
    public Long nuMatricula;
    @Column(name =  "no_aluno")
    public String noAluno;
    @Column(name =  "no_turma")
    public String noTurma;
    
}
