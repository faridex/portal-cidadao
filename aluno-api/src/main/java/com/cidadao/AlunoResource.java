package com.cidadao;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cidadao.entity.Aluno;
import com.cidadao.entity.AlunoBoletim;
import com.cidadao.service.AlunoService;

@Path("/aluno") 
public class AlunoResource {

    @Inject
    AlunoService alunoService; 

    @GET
    @Path("/boletim/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public AlunoBoletim doGetBoletimByFilter(
        @QueryParam("nu_matricula") Long nuMatricula
    ) {
        AlunoBoletim alunoBoletim = alunoService.doGetBoletimByFilter(nuMatricula);
        return alunoBoletim;
    }

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Aluno doGetByFilter(
        @QueryParam("nu_matricula") Long nuMatricula
    ) {
        Aluno aluno = alunoService.doGetByFilter(nuMatricula);
        return aluno;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/load-data")
    public  List<Aluno> doLoadAll(
        @QueryParam("inicio_carca") Long inicio_carga

    ) {
        List<Aluno> alunos = alunoService.doLoadAll(inicio_carga);
        return alunos;
    }

     @GET
    @Produces(MediaType.APPLICATION_JSON)
    public  List<Aluno> doGetAll() {
        List<Aluno> alunos = alunoService.doGetAll();
        return alunos;
    }
}