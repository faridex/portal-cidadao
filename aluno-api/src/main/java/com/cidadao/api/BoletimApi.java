package com.cidadao.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import com.cidadao.entity.Boletim;



@Path("/boletim")
@RegisterRestClient
@ApplicationScoped
public interface BoletimApi {

    @GET
    @Path("/boletim/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Boletim doGetBoletimByFilter(
        @QueryParam("nu_matricula") Long nuMatricula
    ) ;
}
