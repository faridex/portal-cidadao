package com.cidadao;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import com.cidadao.entity.Endereco;
import com.cidadao.service.EnderecoService;

@Path("/endereco") 
public class EnderecoResource {

    @Inject
    EnderecoService enderecoService; 

    @GET
    @Path("/filtros")
    @Produces(MediaType.APPLICATION_JSON)
    public Endereco doGetByFilter(
        @QueryParam("cep") String cep
    ) {
        Endereco endereco = enderecoService.doGetByFilter(cep);
        return endereco;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public  List<Endereco> doGetAll() {
        List<Endereco> enderecos = enderecoService.doGetAll();
        return enderecos;
    }
}